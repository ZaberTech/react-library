# Zaber React Library

Library of React components for Zaber's applications

## Installation

- From the internal gitlab package registry (installed as `@zaber/react-library`)

    ```
    echo @software-public:registry=https://gitlab.izaber.com/api/v4/packages/npm/ >> .npmrc
    npm i @zaber/react-library@npm:@software-public/react-library
    ```

## Development commands

For all use cases, run `npm install` first.

To build the library for release, use `npm run build`. The code will be available in `/dist` and static docs will be in `/styleguide`

The easiest way to test this in development is to copy it to your `node_modules` directly. So to test use
```
npm run build;  Copy-Item dist -Destination ..\zaber-launcher\node_modules\@zaber\react-library\ -Force -Recurse
```
Or in *nix
```
npm run build; cp -rf ./dist ../zaber-launcher/node_modules/@zaber/react-library/
```
replacing `../zaber-launcher/` with the path to the project you're testing it in.

To run the local server and view the documentation:
- `npm-install-peers` to locally install the peer dependencies, which are only needed to run this package locally. Use `npm install -g npm-install-peers` if you do not have this command.
- `npm run start` to start library development server where you can see some documentation about the available components and examples on how to use them.
