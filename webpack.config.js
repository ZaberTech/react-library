const webpack = require('webpack');

module.exports = {
  module: {
    rules: [
      { test: /\.tsx?$/, loader: 'ts-loader' },

      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          'resolve-url-loader',
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ]
      },
      {
        test: /\.(woff2?)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        type: 'asset/inline',
      },
      {
        test: /\.(ttf|eot|png|jpe?g)(\?[\s\S]+)?$/,
        loader: 'asset/resource',
        options: {
          esModule: false,
        },
      },
      {
        test: /\.svg$/,
        issuer: /\.[jt]sx?$/,
        use: ['@svgr/webpack']
      },
      {
        test: /\.template$/,
        type: 'asset/source',
      }
    ]
  },
  externals: {
    'react': 'react',
    'react-dom': 'react-dom',
  },
  plugins: [
    new webpack.EnvironmentPlugin(['NODE_ENV']),
  ],
  resolve: {
    fallback: {
      os: require.resolve('os-browserify/browser'),
    }
  }
};
