const tsEslint = require('typescript-eslint');
const { includeIgnoreFile } = require('@eslint/compat');
const path = require('node:path');
const { config: zaberEslintConfig } = require('@zaber/eslint-config');

module.exports = tsEslint.config(
  includeIgnoreFile(path.resolve(__dirname, '.gitignore')),
  {
    files: ['**/*.js'],
    extends: [zaberEslintConfig.javascript]
  },
  {
    files: ['**/*.{ts,tsx}'],
    extends: [zaberEslintConfig.typescript()]
  },
  {
    files: ['**/*.tsx'],
    extends: [zaberEslintConfig.react]
  },
);
