const path = require('path');

module.exports = {
  sections: [
    {
      name: '',
      content: 'docs/introduction.md'
    },
    {
      name: 'UI Elements',
      components: 'src/elements/**/*.tsx',
    },
    {
      name: 'Layout',
      components: 'src/layout/**/*.tsx',
      exampleMode: 'expand',
      usageMode: 'expand',
    },
    {
      name: 'Animations',
      content: 'src/misc/animations/Animations.md',
    },
    {
      name: 'Palette',
      content: 'src/misc/palette/Palette.md',
    },
    {
      name: 'Colors',
      content: 'src/misc/palette/Colors.md',
    },
  ],
  propsParser: require('react-docgen-typescript').withDefaultConfig().parse,
  require: [
    path.join(__dirname, 'src/index.scss')
  ],
  skipComponentsWithoutExample: true,
};
