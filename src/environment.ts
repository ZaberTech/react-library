export const environment = {
  isTest: typeof process !== 'undefined' && typeof process.env !== 'undefined' && !!process.env.JEST_WORKER_ID,
};
