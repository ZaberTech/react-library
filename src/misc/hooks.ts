import React, { useEffect, useRef } from 'react';

/**
 * Attaches DOM wheel event to an element.
 * The DOM event allows to prevent scrolling by calling preventDefault.
 */
export function useMouseWheel(ref: React.RefObject<HTMLElement>, onWheel: (e: WheelEvent) => void) {
  const onWheelRef = useRef<(e: WheelEvent) => void>();
  onWheelRef.current = onWheel;
  useEffect(() => {
    function onWheel(e: WheelEvent) {
      onWheelRef.current?.(e);
    }

    const element = ref.current;
    if (!element) {
      throw new Error('The passed ref must have initialized value.');
    }
    // In standard react event handler preventDefault won't stop scrolling.
    element.addEventListener('wheel', onWheel);
    return () => element.removeEventListener('wheel', onWheel);
  }, [ref]);
}
