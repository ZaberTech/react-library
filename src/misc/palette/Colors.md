There is also Color enum for inline styling where necessary.

``` jsx padded
import { Colors } from './Colors';

<>{Object.entries(Colors).map(([name, color]) => (
  <div key={name} style={{ display: 'flex', alignItems: 'center' }}>
    <div style={{ width: '30px', height: '30px', margin: '0.2rem', marginRight: '1rem', backgroundColor: color }}/>{name}&emsp;{color}
  </div>
))}</>
```
