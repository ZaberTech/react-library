Our base color palette:

``` jsx padded
<React.Fragment>
  {[
    'white',
    'zaber-white',
    'almost-white',
    'pale-grey',
    'light-grey',
    'grey',
    'zaber-grey',
    'dark-grey',
    'black',
    'green',
    'dark-blue',
    'blue',
    'pale-blue',
    'yellow',
    'orange',
    'pink',
    'bright-red',
    'zaber-red',
    'just-red',
    'dark-red',
  ].map(color => (
    <div key={color} style={{ display: 'flex', alignItems: 'center' }}>
      <div className={`palette-${color}`} style={{ width: '30px', height: '30px', margin: '0.2rem', marginRight: '1rem' }}/>{color}
    </div>
  ))}
</React.Fragment>
```

Colors derived from base color palette:

``` jsx padded
<React.Fragment>
  {[
    'pale-red',
    'error-note',
    'error-warning',
    'error-fault',
  ].map(color => (
    <div key={color} style={{ display: 'flex', alignItems: 'center' }}>
      <div className={`palette-${color}`} style={{ width: '30px', height: '30px', margin: '0.2rem', marginRight: '1rem' }}/>{color}
    </div>
  ))}
</React.Fragment>
```

Extra colors not defined in the color palette

``` jsx padded
<React.Fragment>
  {[
    'pale-green',
    'input-background',
    'interaction-grey-l1',
    'interaction-grey-l2',
    'interaction-white-l1',
    'interaction-white-l2',
  ].map(color => (
    <div key={color} style={{ display: 'flex', alignItems: 'center' }}>
      <div className={`palette-${color}`} style={{ width: '30px', height: '30px', margin: '0.2rem', marginRight: '1rem' }}/>{color}
    </div>
  ))}
</React.Fragment>
```
