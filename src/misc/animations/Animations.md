We have predefined classes which make elements animated. It's useful with icons.

```jsx
import { AnimationClasses } from './animations.ts';
import { Icons } from '../../elements/Icons/Icons';

<>
  <div className={AnimationClasses.Rotation} style={{ width: 'fit-content', margin: '2rem' }}>Hello</div>
  <Icons.Dot className={AnimationClasses.Blink} style={{ color: '#d32024' }}/>
</>
```
