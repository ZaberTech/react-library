export enum AnimationClasses {
  Rotation = 'rotation-animation',
  Blink = 'blink-animation',
}
