export function getZIndex(element: HTMLElement | null): number {
  if (element == null) {
    return 0;
  }
  const zIndex = Number.parseInt(getComputedStyle(element).zIndex, 10);
  if (Number.isNaN(zIndex)) {
    return getZIndex(element.parentElement);
  }
  return zIndex;
}
