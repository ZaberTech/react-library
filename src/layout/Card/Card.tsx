import React, { DetailedHTMLProps } from 'react';
import classNames from 'classnames';

export interface CardProps extends DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  edge?: 'shadow' | 'outline' | 'none';
}

/**
 * Surfaces that display content and actions on a single topic.
 */
export const Card: React.FC<CardProps> = ({ className, children, edge = 'shadow', ...props }: CardProps) => {
  const shadowed = edge === 'shadow';
  const outlined = edge === 'outline';
  return (
    <div className={
      classNames({
        card: true,
        shadowed,
        outlined,
      }, className)}
    {...props}>
      {children}
    </div>
  );
};
