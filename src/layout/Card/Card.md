Simple example of how a card looks like. Cards should be placed on elements with `zaber-white` or `#F8F8F8` as their background:

```jsx padded
<div style={{padding: '2rem', backgroundColor: '#F8F8F8' }}>
  <Card style={{ width: '300px', height: '200px', margin: 'auto' }}>
    Hello
  </Card>
</div>
```

Cards can also have an outline instead of a shadow:
```jsx padded
<div style={{padding: '2rem', backgroundColor: '#F8F8F8' }}>
  <Card edge="outline" style={{ width: '300px', height: '200px', margin: 'auto' }}>
    Hello
  </Card>
</div>
```

Or nothing at all:
```jsx padded
<div style={{padding: '2rem', backgroundColor: '#F8F8F8' }}>
  <Card edge="none" style={{ width: '300px', height: '200px', margin: 'auto' }}>
    Hello
  </Card>
</div>
```