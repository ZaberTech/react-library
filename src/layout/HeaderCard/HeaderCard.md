Make a card with a header:

```jsx padded
<div style={{padding: '2rem', backgroundColor: '#F8F8F8' }}>
  <HeaderCard header={<span>Header</span>} edge="outline" style={{ width: '300px', margin: 'auto' }}>
    <div style={{ paddingLeft: '1.25rem' }}>
      <p>The main contents of the card</p>
    </div>
  </HeaderCard>
</div>
```

Cards can be collapsible, showing a chevron that can be used to open and close the contents:

```jsx padded
import { Text } from '../../elements/Text/Text';

<div style={{padding: '2rem', backgroundColor: '#F8F8F8' }}>
  <HeaderCard collapsible header={<Text t={Text.Type.H4}>Header</Text>} edge="outline" style={{ width: '300px', margin: 'auto' }}>
    <div style={{ paddingLeft: '1.25rem' }}>
      <p>The main contents of the card</p>
      <p>If you get tired of looking at this, close it with the chevron</p>
    </div>
  </HeaderCard>
</div>
```

The `collapsedByDefault` property can be set if the card's content should not be visible right away:

```jsx padded
import { Text } from '../../elements/Text/Text';

<div style={{padding: '2rem', backgroundColor: '#F8F8F8' }}>
  <HeaderCard collapsible header={<Text t={Text.Type.H4}>Header</Text>} collapsedByDefault={true} style={{ width: '300px', margin: 'auto' }}>
    <div style={{ padding: '1.25rem' }}>
      <p>You can't see this until you click on the chevron</p>
    </div>
  </HeaderCard>
</div>
```

Can also be used as controlled component:

```jsx padded
import { Text } from '../../elements/Text/Text';

const [expanded, setExpanded] = React.useState(false);

<div style={{padding: '2rem', backgroundColor: '#F8F8F8' }}>
  <button onClick={() => setExpanded(!expanded)}>Toggle</button>

  <HeaderCard collapsible header={<Text t={Text.Type.H4}>Header</Text>} expanded={expanded} onToggle={setExpanded} style={{ width: '300px', margin: 'auto' }}>
    <div style={{ padding: '1.25rem' }}>
      <p>Content....</p>
    </div>
  </HeaderCard>
</div>
```
