import React, { Component } from 'react';
import classNames from 'classnames';
import { omit } from '@zaber/toolbox';

import { Chevron } from '../../elements/Chevron/Chevron';
import { Card, CardProps } from '../Card/Card';

interface Props extends CardProps {
  header: React.ReactNode;

  collapsible?: boolean;
  titleCollapsed?: string;
  titleExpanded?: string;
  collapsedByDefault?: boolean;

  expanded?: boolean;
  onToggle?: (expanded: boolean) => void;
}

interface State {
  expanded: boolean;
}

/**
 * A card with a header
 */
export class HeaderCard extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      expanded: !props.collapsedByDefault,
    };
  }

  static getDerivedStateFromProps(props: Props, state: State): Partial<State> | null {
    if (props.expanded != null && props.expanded !== state.expanded) {
      return { expanded: props.expanded };
    }
    return null;
  }

  toggleExpanded = () => {
    const { onToggle } = this.props;
    const { expanded } = this.state;
    if (this.props.expanded == null) {
      this.setState({ expanded: !expanded });
    }
    onToggle?.(!expanded);
  };

  renderToggle = () => (
    <Chevron expanded={this.state.expanded}
      titleCollapsed={this.props.titleCollapsed}
      titleExpanded={this.props.titleExpanded}
      onClick={this.toggleExpanded}
    />
  );

  renderHeader = () => (
    <div className="header">
      {this.props.collapsible && this.renderToggle()}
      {this.props.header}
    </div>
  );

  renderContents = () => {
    if (this.state.expanded) {
      return <div className="contents">{this.props.children}</div>;
    } else {
      return null;
    }
  };

  render(): JSX.Element {
    const { className, ...rest } = this.props;
    const cardProps = omit(
      rest, 'header', 'collapsible',  'children', 'titleCollapsed', 'titleExpanded', 'expanded', 'collapsedByDefault', 'onToggle'
    );
    return (
      <Card
        className={classNames('header-card', className)}
        {...cardProps}
      >
        {this.renderHeader()}
        {this.renderContents()}
      </Card>
    );
  }
}
