/* eslint-disable react/display-name */
import classNames from 'classnames';
import React, { forwardRef } from 'react';

type Props = React.DetailedHTMLProps<React.BaseHTMLAttributes<HTMLDivElement>, HTMLDivElement>;

const Row = forwardRef<HTMLDivElement, Props>(({ children, className, ...rest }, passedRef) => (
  <div className={classNames(className, 'flex-component', 'row')} ref={passedRef} {...rest}>
    {children}
  </div>
));

const Column = forwardRef<HTMLDivElement, Props>(({ children, className, ...rest }, passedRef) => (
  <div className={classNames(className, 'flex-component', 'column')} ref={passedRef} {...rest}>
    {children}
  </div>
));

const Spacer = forwardRef<HTMLDivElement>((_, passedRef) => <div className="spacer" ref={passedRef}/>);

export const Flex = {
  Row,
  Column,
  Spacer,
};
