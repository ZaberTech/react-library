
Use `Flex.Row` and `Flex.Col` to quickly create a div using flex display, and `Flex.Spacer` to create white space within:

```jsx padded
import { Flex } from './Flex';
import { Button } from '../../elements/Button/Button';
<Flex.Row>
  <Button color="grey">Cancel</Button>
  <Flex.Spacer/>
  <Button color="red">Do it!</Button>
</Flex.Row>
```
