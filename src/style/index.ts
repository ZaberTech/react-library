/**
 * Contains constants for some components styled by code rather than CSS.
 */
export const styleObject = {
  generalRadius: '0.5rem',
  generalHeight: '2.25rem',
};
