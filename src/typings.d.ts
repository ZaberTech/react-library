declare module '*.svg' {
  const content: new () => React.Component<React.DetailedHTMLProps<React.HTMLAttributes<HTMLOrSVGElement>, HTMLOrSVGElement>>;
  export default content;
}
