import { OptionTypeBase, Styles, mergeStyles as mergeStylesOrig } from 'react-select';

import { Colors } from '../../misc/palette/Colors';
import { styleObject } from '../../style';
import { environment } from '../../environment';

type MockStyles = Record<string, (base: unknown, props: unknown) => unknown>;

/** Basically the original implementation but mocked to make it easier to mock entire react-select. */
export function mergeStylesMock(source: MockStyles, target: MockStyles = {}) {
  const styles = { ...source };
  Object.keys(target).forEach(key => {
    if (source[key]) {
      styles[key] = (rsCss, props) => target[key](source[key](rsCss, props), props);
    } else {
      styles[key] = target[key];
    }
  });
  return styles;
}

const mergeStyles: typeof mergeStylesOrig = environment.isTest ? (mergeStylesMock as typeof mergeStylesOrig) : mergeStylesOrig;

function getDefaultStyles<T extends OptionTypeBase, isMulti extends boolean = false>(
  overrides: Styles<T, isMulti> = {},
): Readonly<Styles<T, isMulti>> {
  return mergeStyles({
    control: (base, { isDisabled }) => {
      const style: typeof base = {
        ...base,
        'height': styleObject.generalHeight,
        'borderRadius': styleObject.generalRadius,
        'minHeight': undefined,
        'borderColor': !isDisabled ? Colors.lightGrey : Colors.paleGrey,
        'backgroundColor': Colors.white,
        'boxShadow': undefined,
        '&:hover': { borderColor: Colors.zaberGrey },
      };
      return style;
    },
    option: (base, { isFocused, isSelected }) => {
      const style = {
        ...base,
        padding: '0.625rem 0.5rem',
        color: Colors.zaberGrey,
      };
      if (isSelected) {
        style.backgroundColor = Colors.almostWhite;
      } else if (isFocused) {
        style.backgroundColor = Colors.zaberWhite;
      }
      return style;
    },
    menu: base => ({
      ...base,
      margin: '0.25rem 0',
    }),
    menuList: base => ({ ...base, padding: '0.5rem' }),
    input: (base, { isDisabled }) => ({ ...base, color: !isDisabled ? Colors.zaberGrey : Colors.paleGrey }),
    placeholder: (base, { isDisabled }) => ({ ...base, color: !isDisabled ? Colors.zaberGrey : Colors.paleGrey }),
    indicatorsContainer: base => ({ ...base, height: '100%' }),
    dropdownIndicator: (base, { isDisabled }) => ({
      ...base,
      'color': !isDisabled ? Colors.lightGrey : Colors.paleGrey,
      '&:hover': { color: Colors.zaberGrey },
    }),
    valueContainer: (base, { isMulti, hasValue }) => ({
      ...base,
      padding: isMulti && hasValue ? '0 0.25rem' : '0 0.5rem',
      marginRight: '0.125rem',
    }),
    multiValue: base => ({
      ...base,
      color: Colors.zaberGrey,
      backgroundColor: Colors.almostWhite,
      borderRadius: '0.25rem',
    }),
    multiValueLabel: base => ({
      ...base,
      fontSize: '1rem',
    }),
    multiValueRemove: base => ({
      ...base,
      'color': Colors.lightGrey,
      'backgroundColor': Colors.almostWhite,
      'borderRadius': '0.25rem',
      ':hover': {
        ...base[':hover'],
        color: Colors.zaberGrey,
        backgroundColor: Colors.almostWhite,
      },
    }),
    clearIndicator: base => ({
      ...base,
      'color': Colors.lightGrey,
      '&:hover': { color: Colors.zaberGrey },
    }),
  }, overrides);
}

export const reactSelect = {
  getDefaultStyles,
  mergeStyles,
};
