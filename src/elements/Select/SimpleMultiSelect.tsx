import classNames from 'classnames';
import React from 'react';
import { components } from 'react-select';
import { omit } from '@zaber/toolbox';

import { environment } from '../../environment';
import { Icons } from '../Icons/Icons';

import { OMIT_FROM_DOM_SELECT, SimpleSelectBase } from './SimpleSelectBase';

const ACCEPTED_TYPES = ['string', 'number'];

const MultiValueContainer: typeof components.MultiValueContainer = ({ innerProps, ...rest }) => (
  <components.MultiValueContainer
    innerProps={{
      ...innerProps,
      className: classNames(innerProps.className, 'simple-select-multi-value'),
    }}
    {...rest}/>
);

const MultiValueRemove: typeof components.MultiValueRemove = ({ ...rest }) => (
  <components.MultiValueRemove
    {...omit(rest, 'children')}>
    <Icons.Cross className="simple-select-clear"/>
  </components.MultiValueRemove>
);

// eslint-disable-next-line import-x/export
export const SimpleMultiSelect = <T extends number | string = string>({
  values, onValuesChange,
  options,
  ...rest
}: SimpleMultiSelect.Props<T>) => {
  const current = values?.map(value => options.find(option => option.value === value)).filter(o => o != null) ?? [];

  if (values?.some(value => !ACCEPTED_TYPES.includes(typeof value))) {
    throw new Error(`Some value is not a string or number (got ${values.join()})`);
  }

  return environment.isTest ? (
    <select
      multiple
      data-current={current.map(option => option.value).join()}
      value={current.map(option => String(option.value))}
      onChange={e => {
        const selected = Array.from(e.target.selectedOptions).map(option => option.value as T);
        onValuesChange(selected);
      }}
      {...omit(rest, ...OMIT_FROM_DOM_SELECT)}>
      {options.map(option => <option key={option.value} value={option.value}>{option.label}</option>)}
    </select>
  ) : (
    <SimpleSelectBase<T, true>
      isMulti
      value={current}
      options={options}
      onChange={options => onValuesChange(options.map(option => option.value) as T[])}
      components={{
        MultiValueContainer,
        MultiValueRemove,
      }}
      {...rest}
    />);
};


// eslint-disable-next-line import-x/export
export declare namespace SimpleMultiSelect {
  export interface Props<T> extends SimpleSelectBase.CommonProps<T, true> {
    values: T[] | null | undefined;
    onValuesChange: (value: T[]) => void;
  }
}
