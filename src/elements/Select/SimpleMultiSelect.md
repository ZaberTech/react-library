Basically a SimpleSelect with multiple values. No icon support yet.

```jsx
import { SimpleMultiSelect } from './SimpleMultiSelect';

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];

const [values, setValues] = React.useState(null);

<div style={{ width: '32rem' }}>
  <SimpleMultiSelect options={options} values={values} onValuesChange={setValues} />
</div>
```
