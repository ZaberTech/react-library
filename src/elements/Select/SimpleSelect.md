Provides simple select component that mocks itself into pure HTML select element.
The component wrapper react-select and provides additional convenience features like autosizing, portaling, and more.

Note that there is a wrapper component `Select` that provides features like labeling in a style of Input component.

```jsx
import { SimpleSelect } from './SimpleSelect';

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];

const [value, setValue] = React.useState(null);

<div style={{ display: 'flex' }}>
  <SimpleSelect options={options} value={value} onValueChange={setValue} />
</div>
```

Here some more fully featured example:

```jsx
import { SimpleSelect } from './SimpleSelect';
import { Icons } from '../Icons/Icons';

const options = [
  { value: 1, label: 'Chocolate', icon: <Icons.Connection/> },
  { value: 2, label: 'Strawberry', icon: <Icons.Flash/> },
  { value: 3, label: 'Vanilla', icon: <Icons.Refresh/> },
];

const [value, setValue] = React.useState(null);

<div style={{ display: 'flex' }}>
  <SimpleSelect
    isSearchable
    classNamePrefix="demo"
    portalToBody
    title="ice cream flavor"
    placeholder="Select flavor"
    blurInputOnSelect
    options={options}
    value={value} onValueChange={setValue} />
</div>
```
