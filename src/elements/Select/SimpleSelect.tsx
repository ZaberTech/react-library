import classNames from 'classnames';
import _ from 'lodash';
import React, { useMemo } from 'react';
import { components } from 'react-select';
import { omit } from '@zaber/toolbox';

import { environment } from '../../environment';

import { reactSelect } from './ReactSelect';
import { OMIT_FROM_DOM_SELECT, SimpleSelectBase } from './SimpleSelectBase';

const ACCEPTED_TYPES = ['string', 'number'];

const WIDTH_PER_CHARACTER = 0.5; // safe empiric value
const ARROW_WIDTH = 2.25;
const WIDTH_MARGIN = 1 + 0.5; // arrow + margins + extra

function calculateWidthFromItems(options: SimpleSelect.Option<unknown>[], hasArrow = true) {
  const maxWordLen = _.max(options.map(o => o.label.length)) ?? 0;
  const hasIcons = options.some(o => o.icon != null);
  return `${maxWordLen * WIDTH_PER_CHARACTER + WIDTH_MARGIN + (hasArrow ? ARROW_WIDTH : 0) + (hasIcons ? 2.5 : 0)}em`;
}

function optionBasedClasses(suffix: string, data: Partial<SimpleSelect.Option<unknown>>) {
  return classNames(`simple-select-${suffix}`, {
    'has-icon': data.icon != null,
    'no-icon': data.icon == null,
  });
}

const OptionComponent: typeof components.Option = ({ className, ...rest }) => {
  const data = rest.data as Partial<SimpleSelect.Option<unknown>>;
  return (
    <components.Option
      className={classNames(className, optionBasedClasses('item', data))}
      {...rest}>
      {data.icon}
      {data.label}
    </components.Option>
  );
};

const SingleValue: typeof components.SingleValue = ({ className, ...rest }) => {
  const { data } = rest;
  return (
    <components.SingleValue
      className={classNames(className, optionBasedClasses('value', data))}
      {...rest}>
      {data.icon}
      {data.label}
    </components.SingleValue>
  );
};

// eslint-disable-next-line import-x/export
export const SimpleSelect = <T extends number | string = string>({
  value, onValueChange,
  options,
  styles,
  calculateMinWidth = true,
  ...rest
}: SimpleSelect.Props<T>) => {
  const current = options.find(option => option.value === value);

  if (value != null && !ACCEPTED_TYPES.includes(typeof value)) {
    throw new Error(`Value is not a string or number (got ${value})`);
  }

  const minWidth = calculateMinWidth ? calculateWidthFromItems(options) : undefined;
  const hasIcons = options.some(o => o.icon != null);
  const mergedStyles = useMemo(() => {
    const defaultStyles = reactSelect.getDefaultStyles<SimpleSelect.Option<T>>({
      control: base => ({
        ...base,
        minWidth,
      }),
      option: base => ({
        ...base,
        padding: hasIcons ? '0 0.5rem 0 0' : '0 0.5rem',
      }),
      valueContainer: base => ({
        ...base,
        padding: hasIcons ?  '0 0.5rem 0 0.25rem' : '0 0.5rem',
      }),
      placeholder: base => ({
        ...base,
        marginLeft: hasIcons ? '0.5rem' : base.marginLeft,
      }),
      input: base => ({
        ...base,
        marginLeft: hasIcons ? '0.5rem' : base.marginLeft,
      }),
    });
    return reactSelect.mergeStyles(defaultStyles, styles ?? {});
  }, [minWidth, hasIcons, styles]);

  return environment.isTest ? (
    <select
      data-current={current?.label ?? ''}
      value={current?.value}
      onChange={e => onValueChange(options.find(o => o.value.toString() === e.target.value)!.value as T)}
      {...omit(rest, ...OMIT_FROM_DOM_SELECT)}>
      {options.map(option => <option key={option.value} value={option.value}>{option.label}</option>)}
    </select>
  ) : (
    <SimpleSelectBase<T, false>
      isMulti={false}
      styles={mergedStyles}
      options={options}
      value={current ?? null}
      onChange={newValue => onValueChange(newValue!.value as T)}
      components={{
        Option: OptionComponent,
        SingleValue,
      }}
      {...rest}
    />);
};

SimpleSelect.calculateWidthFromItems = calculateWidthFromItems;


// eslint-disable-next-line import-x/export
export declare namespace SimpleSelect {
  export interface Option<T> extends SimpleSelectBase.Option<T> {
    icon?: React.ReactNode;
  }

  export interface Props<T> extends SimpleSelectBase.CommonProps<T, false> {
    value: T | null | undefined;
    onValueChange: (value: T) => void;
    options: Option<T>[];

    calculateMinWidth?: boolean;
  }
}
