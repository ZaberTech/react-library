We don't provide a wrapper components because it's unnecessary and `react-select` is quite complex.
Instead we provide a helper object called `reactSelect` that contains styles that adapt it for our applications.

```jsx
import Select from 'react-select';
import { reactSelect } from './ReactSelect';

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];
const styles = reactSelect.getDefaultStyles();

<div style={{ width: '20rem' }}>
  <Select options={options} styles={styles}/>
</div>
```

We also support multi variant:

```jsx
import Select from 'react-select';
import { reactSelect } from './ReactSelect';

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];
const styles = reactSelect.getDefaultStyles();

<div style={{ width: '20rem' }}>
  <Select isMulti options={options} styles={styles}/>
</div>
```

Optionally, extend the styles on the control by passing in a CSS Object

```jsx
import Select from 'react-select';
import { reactSelect } from './ReactSelect';

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];
const styles = reactSelect.getDefaultStyles({
  control: (base, { isDisabled }) => ({
    ...base,
    width: '9rem',
    borderColor: !isDisabled ? base.borderColor : '#00F',
    '&:hover': { borderColor: '#F00' },
  }),
  menu: { width: '20rem', left: '0' }
});

<div style={{ width: '20rem' }}>
  <Select options={options} styles={styles} menuPlacement="auto"/>
  <br/>
  <Select isDisabled={true} options={options} styles={styles} menuPlacement="auto"/>
</div>
```
