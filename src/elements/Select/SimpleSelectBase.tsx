import classNames from 'classnames';
import React, { useLayoutEffect, useMemo, useRef, useState } from 'react';
import ReactSelect, { GroupTypeBase, MenuPlacement, Styles, components } from 'react-select';
import { omit } from '@zaber/toolbox';

import { getZIndex } from '../../misc/utils';
import { Colors } from '../../misc/palette/Colors';
import { Icons } from '../Icons/Icons';

import { reactSelect } from './ReactSelect';
import { SimpleSelect } from './SimpleSelect';

export const OMIT_FROM_DOM_SELECT = [
  'classNamePrefix',
  'blurInputOnSelect',
  'portalToBody',
  'menuPlacement',
  'isSearchable',
] as const;

const OptionComponent: typeof components.Option = ({ className, ...rest }) => {
  const data = rest.data as Partial<SimpleSelect.Option<unknown>>;
  return (
    <components.Option
      className={classNames(className, 'simple-select-item')}
      {...rest}>
      {data.label}
    </components.Option>
  );
};

const Menu: typeof components.Menu = ({ className, ...rest }) => (
  <components.Menu
    className={classNames(className, 'simple-select-menu')}
    {...rest}/>
);

const ClearIndicator: typeof components.ClearIndicator = ({ ...rest }) => (
  <components.ClearIndicator
    {...omit(rest, 'children')}>
    <Icons.Cross className="simple-select-clear"/>
  </components.ClearIndicator>
);

// eslint-disable-next-line import-x/export
export const SimpleSelectBase = <T extends number | string, isMulti extends boolean>({
  styles,
  disabled,
  className,
  menuPlacement = 'auto',
  portalToBody,
  isSearchable = false,
  components,
  ...rest
}: SimpleSelectBase.InternalProps<T, isMulti>) => {
  const selectRef = useRef<ReactSelect<SimpleSelectBase.Option<T>, isMulti> | null>(null);
  const [zIndex, setZIndex] = useState<number | null>(null);
  const [isOpen, setIsOpen] = useState(false);

  const mergedStyles = useMemo(() => {
    const defaultStyles = reactSelect.getDefaultStyles<SimpleSelectBase.Option<T>, isMulti>({
      menuPortal: base => ({
        ...base,
        zIndex: zIndex ?? base.zIndex,
      }),
      placeholder: base => ({
        ...base,
        color: Colors.lightGrey,
        display: (isSearchable && isOpen) ? 'none' : base.display,
      }),
    });
    return reactSelect.mergeStyles(defaultStyles, styles ?? {});
  }, [zIndex, styles]);

  useLayoutEffect(() => {
    const control = selectRef.current?.select?.controlRef;
    if (portalToBody && control) {
      setZIndex(getZIndex(control) + 1);
    }
  }, [portalToBody]);

  return <ReactSelect<SimpleSelectBase.Option<T>, isMulti>
    ref={selectRef}
    className={classNames(className, 'simple-select')}
    styles={mergedStyles}
    menuPlacement={menuPlacement}
    menuPortalTarget={portalToBody ? document.body : undefined}
    isSearchable={isSearchable}
    isDisabled={disabled}
    onMenuOpen={() => setIsOpen(true)}
    onMenuClose={() => setIsOpen(false)}
    components={{
      Option: OptionComponent,
      Menu,
      ClearIndicator,
      ...components,
    }}
    {...rest}
  />;
};


// eslint-disable-next-line import-x/export
export declare namespace SimpleSelectBase {
  export interface Option<T> {
    value: T;
    label: string;
  }

  export interface InternalProps<T, isMulti extends boolean> extends CommonProps<T, isMulti> {
    isMulti: isMulti;
    value: ReactSelect<SimpleSelectBase.Option<T>, isMulti>['props']['value'];
    onChange: ReactSelect<SimpleSelectBase.Option<T>, isMulti>['props']['onChange'];
    components: ReactSelect<SimpleSelectBase.Option<T>, isMulti>['props']['components'];
  }

  export interface CommonProps<T, isMulti extends boolean> {
    options: Option<T>[];
    className?: string;
    classNamePrefix?: string;
    disabled?: boolean;
    title?: string;
    placeholder?: string;
    menuPlacement?: MenuPlacement;
    styles?: Partial<Styles<Option<T>, isMulti, GroupTypeBase<Option<T>>>>;
    id?: string;
    isSearchable?: boolean;
    blurInputOnSelect?: boolean;
    /**
     * Makes the dropdown menu mount to document body.
     * Fixes issues with dropdowns being cut off by modals or overflow:hidden containers.
     */
    portalToBody?: boolean;
  }
}
