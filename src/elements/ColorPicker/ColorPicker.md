Control for choosing RGB colors in HTML-compatible string format.

```jsx
const [color, setColor] = React.useState('#00FF00');

<div style={{ width: '300px' }}>
  <p>{color}</p>
  <ColorPicker color={color} onChange={setColor}/>
</div>
```

There is also an optional `disabled` property to show an inactive state where the popup does not appear.

```jsx
<div style={{ width: '300px' }}>
  <ColorPicker color="#00FF00" disabled onChange={_ => {}}/>
</div>
```


