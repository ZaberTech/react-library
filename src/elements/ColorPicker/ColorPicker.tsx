import React, { useCallback, useRef, useState } from 'react';
import { HexColorPicker } from 'react-colorful';
import useOnClickOutside from 'use-onclickoutside';


export interface Props {
  color: string;
  onChange: (color: string) => void;
  disabled?: boolean;
}


export const ColorPicker: React.FC<Props> = ({ color, onChange, disabled = false }) => {
  const popup = useRef(null);
  const [isOpen, setOpen] = useState(false);

  const close = useCallback(() => setOpen(false), []);
  useOnClickOutside(popup, close);

  return (
    <div className="color-picker">
      <div
        className="swatch"
        style={{
          backgroundColor: disabled ? 'transparent' : color,
          borderColor: color,
        }}
        onClick={() => setOpen(true)}/>

      {isOpen && !disabled && <div className="popup" ref={popup}>
        <HexColorPicker color={color} onChange={onChange}/>
      </div>}
    </div>
  );
};
