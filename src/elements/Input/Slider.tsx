import React, { useMemo, useRef, DetailedHTMLProps } from 'react';
import classNames from 'classnames';
import _ from 'lodash';

import { Text } from '../Text/Text';
import { useMouseWheel } from '../../misc/hooks';

function calculateWidthPercentage(value: number, min: number, max: number): string {
  return `${((value - min) / (max - min) * 100).toString()}%`;
}

interface TickIntervals {
  major: number;
  minor?: number;
}

interface Props extends DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
  max: number;
  min: number;
  value: number;
  step?: number;
  maxLabel?: string;
  minLabel?: string;
  showTicks?: boolean;
  tickIntervals?: TickIntervals;
  onValueChange?: (value: number) => void;
  allowMouseWheel?: boolean;
}

export const Slider: React.FC<Props> = ({
  className, onValueChange, min, max, value, step = 1,
  maxLabel, minLabel, disabled,
  showTicks = false, tickIntervals, allowMouseWheel = false,
  ...rest
}) => {
  const sliderMin = Number(min);
  const sliderMax = Number(max);
  const labelZero = sliderMin < 0 && sliderMax > 0;

  const tickList = useMemo(() => {
    const newTickList = [];
    if (showTicks) {
      if (!tickIntervals) {
        for (let i = sliderMin; i <= sliderMax; i += step) {
          newTickList.push({
            value: i, isMajor: Number.isInteger(i),
            percentage: calculateWidthPercentage(i, sliderMin, sliderMax)
          });
        }
      } else {
        for (let i = sliderMin; i <= sliderMax; i += step) {
          if (Number.isInteger(i / tickIntervals.major)) {
            newTickList.push({
              value: i, isMajor: true,
              percentage: calculateWidthPercentage(i, sliderMin, sliderMax)
            });
          } else if (tickIntervals.minor && Number.isInteger(i / tickIntervals.minor)) {
            newTickList.push({
              value: i, isMajor: false,
              percentage: calculateWidthPercentage(i, sliderMin, sliderMax)
            });
          }
        }
      }
    }
    return newTickList;
  }, [sliderMin, sliderMax, showTicks]);

  const mainDivRef = useRef<HTMLDivElement>(null);
  useMouseWheel(mainDivRef, e => {
    if (disabled || !allowMouseWheel) { return }

    e.preventDefault();

    let newValue = value + (e.deltaY > 0 ? -step : step);
    const stepDecimals = Math.floor(Math.log10(step));
    newValue = _.round(newValue, -stepDecimals);
    onValueChange?.(Math.min(Math.max(newValue, sliderMin), sliderMax));
  });

  return <div ref={mainDivRef} className={classNames('input-slider', className, { disabled })}>
    <div className="slider-container">
      <div className="slider-value">
        <div style={{ width: calculateWidthPercentage(value, sliderMin, sliderMax) }}/>
        <div className="value-container"><Text>{value}</Text></div>
      </div>

      <input type="range" className="slider" step={step} value={value}
        onChange={e => {
          if (!disabled) {
            onValueChange?.(Number(e.target.value));
          }
        }} min={sliderMin} max={sliderMax}
        list={showTicks ? 'showTicks' : undefined} disabled={disabled} {...rest}/>

      {showTicks && tickList.length > 0 && <svg className="tick-marks">
        {tickList.map((tick, index) =>
          <rect key={index} className={classNames('range-tick', tick.isMajor ? 'major' : 'minor')}
            x={tick.percentage} y="0">
          </rect>)}
      </svg>}
    </div>

    <div className="label-container">
      <div className="text-aligner left"><Text e={Text.Emphasis.Light}>{minLabel ?? min}</Text></div>
      <div style={{ flex: labelZero ? Math.abs(sliderMin) : 1 }}></div>
      {labelZero && <div className="text-aligner zero"><Text e={Text.Emphasis.Light}>0</Text></div>}
      {labelZero && <div style={{ flex: sliderMax }}></div>}
      <div className="text-aligner right"><Text e={Text.Emphasis.Light}>{maxLabel ?? max}</Text></div>
    </div>
  </div>;
};
