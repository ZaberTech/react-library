Custom styled input slider, user can choose with or without tick marks,
and specify the multiple for major and minor ticks. By default the slider shows
major tick marks at integer values and minor tick marks at non-integer values.

Note: Having too many tick marks can negatively impact the responsiveness and aesthetics of the
component, so tickIntervals should be adjusted to reasonable values based on the context the component is used in.

```jsx
const [value, setValue] = React.useState(5);

<div style={{ width: '26rem' }}>
  <Slider min={0} max={10} step={0.5} value={value} onValueChange={setValue} showTicks allowMouseWheel/>
  <Slider min={0} max={10} step={2} value={6}/>
  <Slider min={-10} max={5} step={1} showTicks value={5}/>
  <Slider min={0} max={100} step={0.5} value={50} showTicks tickIntervals={{ major: 20, minor: 10 }}/>
  <Slider min={0} max={10} value={2} showTicks disabled/>
</div>
```

Set your own labels with `maxLabel` and `minLabel`

```jsx
const [value, setValue] = React.useState(0.5);

<div style={{ width: '26rem' }}>
  <Slider min={0} max={1} step={0.01} value={value} onValueChange={setValue} minLabel="smöl" maxLabel="Bigly" allowMouseWheel/>
</div>
```
