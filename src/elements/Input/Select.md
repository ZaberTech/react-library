Wraps SimpleSelect component adding typical input functionality.

```jsx
import { Select } from './Select';

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];

const [value, setValue] = React.useState('chocolate');

<div style={{ display: 'flex' }}>
  <Select
    labelContent="Flavor"
    options={options} value={value}
    status="warning"
    message="You are unlikely to find this flavor in the store."
    onChange={option => setValue(option.value)} />
</div>
```
