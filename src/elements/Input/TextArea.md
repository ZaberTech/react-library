Create a text area with multiple lines of input

```jsx
<div style={{ width: '300px' }}>
  <TextArea labelContent="Feedback" rows="2" cols="30"/>
</div>
```
