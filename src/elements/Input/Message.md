Inputs can have messages below them for added info:

```jsx
import { Input } from './Input';

const [errUp, setErrUp] = React.useState(false);

<div style={{ width: '300px' }}>
  <Input labelContent="Input" message="This input has a message!"/>
  <Input labelContent="Input" status="warning" message="This input is questionable"/>
  <Input
    labelContent="Type to see message" status="error" message="This input is bad" hideMessage={!errUp}
    onChange={value => setErrUp(!!value)}
  />
</div>
```