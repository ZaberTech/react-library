`NumericInput` is just like `Input` except
it provides only numbers through `onNumberChange` callback (or `null` when the input is invalid).

```jsx
const [num, setNum] = React.useState(0);

<div style={{ width: '300px' }}>
  <NumericInput labelContent="Your Number Here" value={num} onNumberChange={setNum}/>
  <p>The current value: {num == null ? 'invalid' : num.toPrecision(4)}</p>
</div>
```

You can provide `defaultValue` that will be used instead of `null` when the input is invalid.

```jsx
const [num, setNum] = React.useState(0);

<div style={{ width: '300px' }}>
  <NumericInput defaultValue={123} value={num} onNumberChange={setNum}/>
  <p>The current value: {num == null ? 'invalid' : num}</p>
</div>
```
