import React, { DetailedHTMLProps, useMemo } from 'react';
import classNames from 'classnames';
import _ from 'lodash';
import { omit } from '@zaber/toolbox';

import CheckedSvg from '../../../assets/img/checkbox_on.svg';
import UncheckedSvg from '../../../assets/img/checkbox_off.svg';
import { iconFromSvg } from '../SvgIcon/SvgIcon';
import { Text } from '../Text/Text';

const CheckedIcon = iconFromSvg(CheckedSvg);
const UncheckedIcon = iconFromSvg(UncheckedSvg);

interface Props extends DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
  labelContent?: React.ReactNode;
  bold?: boolean;
  onChecked: (checked: boolean) => void;
}


export const Checkbox: React.FC<Props> = props => {
  const { id, checked, className, labelContent, bold = false, children, disabled, ...rest } = omit(props, 'onChange', 'onChecked');
  const useId = useMemo(() => id ?? _.uniqueId('zaber-lib-checkbox'), [id]);

  const onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { onChange, onChecked } = props;
    if (onChange) {
      onChange(e);
    }
    if (onChecked) {
      onChecked(e.target.checked);
    }
  };

  return (
    <div className={classNames('input-checkbox', { disabled }, className)}>
      <input type="checkbox" id={useId} checked={checked} onChange={onChange} disabled={disabled} {...rest}/>
      <label htmlFor={useId}>
        {checked && <CheckedIcon className={classNames('checkbox-svg', 'checked')}/>}
        {!checked && <UncheckedIcon className={classNames('checkbox-svg', 'unchecked')}/>}
        {labelContent && <Text className={classNames('checkbox-label-text', bold && 'checkbox-bold')}>{labelContent}</Text>}
      </label>
      {children && <div className="checkbox-description">
        {children}
      </div>}
    </div>
  );
};
