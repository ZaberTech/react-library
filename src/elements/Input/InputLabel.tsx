import classNames from 'classnames';
import React from 'react';

import { HelpTooltip } from '../HelpTooltip/HelpTooltip';
import { Text, TextEmphasis, TextFamily, TextType } from '../Text/Text';

interface InputLabelProps {
  label: string;
  help?: React.ReactNode;
  t?: TextType;
  e?: TextEmphasis;
  f?: TextFamily;
}

const InputLabelRow: React.FC<InputLabelProps & { htmlFor: string }> = props => {
  const { label, htmlFor, help, t = TextType.Helper, e, f } = props;
  return <Text className="input-label-row" {...{ t, e: e ?? TextEmphasis.Light, f }}>
    <label className="input-label-label" htmlFor={htmlFor}>{label}</label>
    {help && <HelpTooltip className={classNames('input-label-help', { emphasized: e != null })}>{help}</HelpTooltip>}
  </Text>;
};

type InputLabelContent = string | InputLabelProps;

// eslint-disable-next-line @typescript-eslint/no-namespace, import-x/export
export namespace InputLabel {
  export type Content = InputLabelContent;
}


// eslint-disable-next-line import-x/export
export const InputLabel: React.FC<{ content?: InputLabelContent; htmlFor: string }> = ({ content, htmlFor }) => {
  if (!content) {
    return null;
  } else if (typeof content === 'string') {
    return <InputLabelRow label={content} htmlFor={htmlFor}/>;
  } else {
    return <InputLabelRow {...content} htmlFor={htmlFor}/>;
  }
};
