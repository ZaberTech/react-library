/* eslint-disable react/display-name */
import React, { forwardRef, useMemo } from 'react';
import classNames from 'classnames';
import _ from 'lodash';
import { omit } from '@zaber/toolbox';

import { InputLabel } from './InputLabel';

interface TextAreaProps extends Omit<
  React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLTextAreaElement>, HTMLTextAreaElement>,
  'crossOrigin' | 'onPointerEnterCapture' | 'onPointerLeaveCapture'> {
  labelContent?: InputLabel.Content;
  onValueChange?: (value: string) => void;
  rows?: number;
}

export const TextArea = forwardRef<HTMLTextAreaElement, TextAreaProps>((props, passedRef) => {
  const { id, className, labelContent, ...rest } = omit(props, 'onChange', 'onValueChange');
  const useId = useMemo(() => id ?? _.uniqueId('zaber-lib-textarea'), [id]);

  const onChange = (e: React.ChangeEvent<HTMLTextAreaElement>): void => {
    const { onChange, onValueChange } = props;

    if (onChange) {
      onChange(e);
    }

    if (onValueChange) {
      onValueChange(e.target.value);
    }
  };

  return (
    <div className={classNames('input', className)}>
      <InputLabel content={labelContent} htmlFor={useId}/>
      <textarea ref={passedRef} id={useId} onChange={onChange} {...rest}/>
    </div>
  );
});
