We have custom styled input with label.

```jsx
<div style={{ width: '300px' }}>
  <Input labelContent="Hostname"/>
  <Input type="number" labelContent="Port"/>
</div>
```

If the `clearable` prop is set, an "X" will appear to the right of the input box when there is any text input allowing it to be cleared.

```jsx
const [val, setVal] = React.useState('Clear me');

<div style={{ width: '300px' }}>
  <Input clearable value={val} labelContent="Clearable" onValueChange={setVal}/>
  <p>The current value: {val}</p>
</div>
```

The ref for the `input` element is forwarded, so it can be managed directly

```jsx
const ref = React.createRef();

<div style={{ width: '300px' }}>
  <Input clearable ref={ref} labelContent="Ref Forwarded"/>
  <button onClick={() => {ref.current.value += 'something'}}>
    Add something
  </button>
</div>
```

Add disabled or status to style the input

```jsx

<div style={{ width: '300px' }}>
  <Input labelContent="Disabled" disabled value="3"/>
  <Input labelContent="Read Only" disabled="readonly" value="6"/>
  <Input labelContent="Error" status="error"/>
  <Input labelContent="Warning" status="warning"/>
</div>
```

There is also custom style for date Input, `DateInput`.

```jsx
import { DateInput } from './Input';

<div style={{ width: '300px' }}>
  <DateInput labelContent="Expiry Date"/>
</div>
```

Inputs can have children:

```jsx
<div style={{ width: '400px' }}>
  <Input value="Value" labelContent="With children"
    inputLineChildren={<span>Inside</span>}>
    <span>Outside</span>
  </Input>
</div>
```

Inputs can also have icons. The size is set by the input and should not be changed.

```jsx
import { Icons } from '../Icons/Icons';

<div style={{ width: '400px' }}>
  <Input value="Value" labelContent="With icon" icon={<Icons.Search/>} />
</div>
```
