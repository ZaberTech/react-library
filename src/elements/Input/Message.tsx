import React from 'react';
import classNames from 'classnames';

import { Text } from '../Text/Text';
import { NoticeIcon } from '../Notice/NoticeIcon';
import { NoticeType } from '../Notice/Notice';

export interface Props {
  status?: NoticeType | null;
  /** A message that will be added below the input, beside an icon determined by the status. */
  message?: React.ReactNode;
  /** If true, the message will not be shown, but it will take up the space it normally would in the DOM to help with formatting */
  hideMessage?: boolean;
}

export const InputMessage: React.FC<Props> = ({ message, hideMessage, status }) => {
  if (!message) {
    return null;
  }
  return (
    <div className={classNames('message-line', status, { hidden: hideMessage })}>
      <NoticeIcon type={status}/>
      <Text className={classNames(status)} t={Text.Type.Helper}>{message}</Text>
    </div>);
};
