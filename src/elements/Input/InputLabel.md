Inputs can have labels. Usually just a string is enough, but sometimes you may want to add a help tooltip too if the name alone is unclear:

```jsx
import { Input } from './Input';

<div style={{ width: '300px' }}>
  <Input value="" labelContent={{ label: 'Unclear Label', help:(<div>
      <p>This tooltip can be used to explain what an input is for using many words and maybe even links and pictures.</p>
      <p>This input for example was created to show you how to use a tooltip. It has no other purpose in this world.</p>
      <p>In fact, it is rather useless as an input. The value can't even be changed :(</p>
    </div>)}}/>
</div>
```

These also take the same styles as `Text`, so you can make the labels look however you want:

```jsx
import { Input } from './Input';
import { Text } from '../Text/Text';

<div style={{ width: '300px' }}>
  <Input value="" labelContent={{
    label: 'Stylish Label',
    t: Text.Type.H3, e: Text.Emphasis.Red, f: Text.Family.Mono,
    help: <Text>Wow, the tooltip is also stylish</Text>
    }}/>
</div>
```
