import _ from 'lodash';
import React, { Component, forwardRef } from 'react';
import { omit } from '@zaber/toolbox';

import { Input } from './Input';

type InputProps = React.ComponentPropsWithoutRef<typeof Input>;

interface Props extends Omit<InputProps, 'type' | 'value' | 'onValueChange'> {
  innerRef: React.Ref<HTMLInputElement>;
  value: number | null;
  onNumberChange: (value: number | null) => void;
  /** Rounds the display value to this precision when a new value is pushed through props */
  displayPrecision?: number;
  /** Value used instead of null when the input has no value. */
  defaultValue?: number;
}

interface State {
  rawValue: string;
}

function getNumericValue(value: string, { defaultValue }: Props): number | null {
  const numericValue = parseFloat(value);
  return Number.isFinite(numericValue) ? numericValue : (defaultValue ?? null);
}

function getStringValue({ value, displayPrecision }: Props): string {
  if (value == null) { return '' }
  const rounded = displayPrecision != null ? _.round(value, displayPrecision) : value;
  return rounded.toString();
}

class NumericInputBase extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      rawValue: getStringValue(props),
    };
  }

  static getDerivedStateFromProps(props: Props, state: State): Partial<State> | null {
    if (getNumericValue(state.rawValue, props) !== props.value || props.disabled) {
      return { rawValue: getStringValue(props) };
    }
    return null;
  }

  onValueChange = (value: string) => {
    const { onNumberChange } = this.props;
    this.setState({ rawValue: value });
    onNumberChange(getNumericValue(value, this.props));
  };

  onBlur = (e: React.FocusEvent<HTMLInputElement, HTMLElement>) => {
    const { value, onBlur } = this.props;
    if (value != null) {
      this.setState({ rawValue: getStringValue(this.props) });
    }

    onBlur?.(e);
  };

  defaultStatus(): 'error' | null {
    const { value, disabled } = this.props;
    if (disabled) { return null }
    return value == null || !Number.isFinite(value) ? 'error' : null;
  }

  render(): React.ReactNode {
    const { innerRef, status, ...rest } = this.props;
    const { rawValue } = this.state;
    return (
      <Input type="number"
        ref={innerRef}
        value={rawValue} onValueChange={this.onValueChange}
        status={status !== undefined ? status : this.defaultStatus()}
        onBlur={this.onBlur}
        {...omit(rest, 'value', 'onNumberChange', 'displayPrecision', 'onBlur', 'defaultValue')}/>
    );
  }
}

// eslint-disable-next-line react/display-name
export const NumericInput = forwardRef<HTMLInputElement, Omit<Props, 'innerRef'>>(
  (props, ref) => <NumericInputBase innerRef={ref} {...props}/>
);
