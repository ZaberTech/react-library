Create a checkbox with our styling

```jsx
const [checked, setChecked] = React.useState(false);
const [anotherChecked, setAnotherChecked] = React.useState(false);

<div style={{ width: '400px' }}>
  <Checkbox labelContent="This can be checked (or not)" checked={checked} onChecked={setChecked} />
  <br/>
  <Checkbox labelContent="This one has children and is bold style" checked={anotherChecked} onChecked={setAnotherChecked} bold>
    Children is aligned to label text
  </Checkbox>
  <br/>
  <Checkbox disabled labelContent="This one is disabled" checked/>
</div>
```
