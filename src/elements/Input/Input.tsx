import React, { forwardRef, useRef, useMemo } from 'react';
import classNames from 'classnames';
import _ from 'lodash';
import { omit } from '@zaber/toolbox';

import { Icons } from '../Icons/Icons';

import { InputMessage, Props as InputMessageProps } from './Message';
import { InputLabel } from './InputLabel';

type InputProps = Omit<
  React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>,
  'crossOrigin' | 'onPointerEnterCapture' | 'onPointerLeaveCapture'
>;

interface Props extends Omit<InputProps & InputMessageProps, 'disabled'> {
  labelContent?: InputLabel.Content;
  inputLineChildren?: React.ReactNode;
  onValueChange?: (value: string) => void;
  clearable?: boolean;
  enterKeyHint?: InputProps['enterKeyHint'];
  icon?: React.ReactNode;
  disabled?: boolean | 'readonly';
}

/** Clears the input and fires a fake event for onChange to handle */
const resetRef = (ref: React.RefObject<HTMLInputElement>) => {
  if (!ref.current) { return }

  const descriptor = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, 'value');
  if (descriptor?.set == null) {
    throw new Error('Cannot clear input value. SimpleSelect value property has no setter for value property.');
  }
  descriptor.set.call(ref.current, '');

  const event = new Event('change', { bubbles: true });
  ref.current.dispatchEvent(event);
};

// eslint-disable-next-line react/display-name
export const Input = forwardRef<HTMLInputElement, Props>((props, passedRef) => {
  const {
    id, clearable, type = 'text', className, labelContent, status, disabled, inputLineChildren, message, hideMessage, children, icon,
    value, ...rest
  } = omit(props, 'onChange', 'onValueChange');
  const innerRef = useRef<HTMLInputElement>(null);
  const useId = useMemo(() => id ?? _.uniqueId('zaber-lib-input'), [id]);

  const onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { onChange, onValueChange } = props;
    if (onChange) {
      onChange(e);
    }
    if (onValueChange) {
      onValueChange(e.target.value);
    }
  };

  const reset = () => {
    if (!passedRef) {
      resetRef(innerRef);
    } else {
      if (typeof passedRef !== 'function') {
        resetRef(passedRef);
      }
    }
  };

  return (
    <div className={classNames('input', disabled === true ? 'disabled' : disabled, className, status && `status-${status}`)}>
      <InputLabel content={labelContent} htmlFor={useId}/>
      <div className="input-row">
        <div className="input-line">
          {icon}
          <input
            id={useId}
            ref={passedRef ?? innerRef}
            type={type}
            onChange={onChange}
            disabled={!!disabled}
            value={value}
            {...rest}
            // Prevents scroll events from editing the value of numeric inputs
            onWheel={e => {
              const target = e.target as HTMLInputElement;
              target.blur();
            }}
          />
          {clearable && value && <Icons.Cross className="clear-input" onClick={reset}/>}
          {inputLineChildren}
        </div>
        {children}
      </div>
      <InputMessage status={status} message={message} hideMessage={hideMessage}/>
    </div>
  );
});

// eslint-disable-next-line react/display-name
export const DateInput = forwardRef<HTMLInputElement, Omit<Props, 'type'>>((props, passedRef) =>
  <Input type="date" {...props} ref={passedRef}/>);
