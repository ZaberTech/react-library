import classNames from 'classnames';
import _ from 'lodash';
import React, { useMemo } from 'react';

import { SimpleSelect } from '../Select/SimpleSelect';
import { NoticeType } from '../Notice/Notice';

import { InputMessage, Props as InputMessageProps } from './Message';
import { InputLabel } from './InputLabel';

export interface Option<T> {
  value: T;
  label: string;
}

export interface Props<T> extends
  Omit<SimpleSelect.Props<T>, 'classNamePrefix' | 'styleOverride'>,
  InputMessageProps
{
  selectClassName?: string;
  labelContent?: InputLabel.Content;
  status?: NoticeType | null;
  /** A message that will be added below the input, beside an icon determined by the status. */
  message?: React.ReactNode;
  /** If true, the message will not be shown, but it will take up the space it normally would in the DOM to help with formatting */
  hideMessage?: boolean;
}

export const Select = <T extends number | string = string>({
  labelContent,
  selectClassName,
  className,
  disabled,
  status,
  message,
  hideMessage,
  ...rest
}: Props<T>) => {
  const useId = useMemo(() => rest.id ?? _.uniqueId('select'), [rest.id]);
  return (
    <div className={classNames('input', { disabled }, className, status && `status-${status}`)}>
      <InputLabel content={labelContent} htmlFor={useId}/>
      <SimpleSelect
        className={selectClassName} classNamePrefix="input-select"
        id={useId} disabled={disabled}
        {...rest}/>
      <InputMessage status={status} message={message} hideMessage={hideMessage}/>
    </div>
  );
};
