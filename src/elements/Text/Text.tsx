import React, { DetailedHTMLProps } from 'react';
import classNames from 'classnames';

export enum TextType {
  H1 = 'h1',
  H2 = 'h2',
  H3 = 'h3',
  H4 = 'h4',
  H5 = 'h5',
  BodySm = 'text-body-small',
  Body = 'text-body',
  BodyLg = 'text-body-large',
  BodyXSm = 'text-body-xsmall',
  Button = 'text-button',
  Helper = 'text-helper',
  Instruction = 'text-instruction',
  Overline = 'text-overline',
}

export enum TextEmphasis {
  Red = 'red',
  Regular = 'regular',
  Light = 'light',
  XLight = 'x-light',
  Bold = 'bold',
}

export enum TextFamily {
  Sans = 'sans',
  Mono = 'mono',
}

interface TextProps extends DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLSpanElement>, HTMLSpanElement> {
  t?: TextType;
  e?: TextEmphasis;
  f?: TextFamily;
  appearsClickable?: boolean;
}

export const Text: React.FC<TextProps> & { Type: typeof TextType; Emphasis: typeof TextEmphasis; Family: typeof TextFamily } =
({ t, e, f, appearsClickable, className, ...props }) => (
  <span
    className={classNames(
      t ?? TextType.Body,
      e ?? TextEmphasis.Regular,
      f ?? TextFamily.Sans,
      { clickable: appearsClickable },
      className,
      'text-span',
    )}
    {...props}
  />
);

Text.Type = TextType;
Text.Emphasis = TextEmphasis;
Text.Family = TextFamily;
