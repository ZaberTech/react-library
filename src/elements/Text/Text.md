
You can use different sizes:

```jsx padded
<div><Text t={Text.Type.H1}>Hello! (H1)</Text></div>
<div><Text t={Text.Type.H2}>Hello! (H2)</Text></div>
<div><Text t={Text.Type.H3}>Hello! (H3)</Text></div>
<div><Text t={Text.Type.H4}>Hello! (H4)</Text></div>
<div><Text t={Text.Type.H5}>Hello! (H5)</Text></div>
<div><Text t={Text.Type.BodyXLg}>Hello! (Extra Body Large)</Text></div>
<div><Text t={Text.Type.BodyLg}>Hello! (Body Large)</Text></div>
<div><Text t={Text.Type.Body}>Hello! (Body)</Text></div>
<div><Text t={Text.Type.BodySm}>Hello! (Body Small)</Text></div>
<div><Text t={Text.Type.Button}>Hello! (Button)</Text></div>
<div><Text t={Text.Type.Helper}>Hello! (Helper)</Text></div>
<div><Text t={Text.Type.Instruction}>Hello! (Instruction)</Text></div>
<div><Text t={Text.Type.Overline}>Hello! (Overline)</Text></div>
```

You can use different emphasis:

```jsx padded
<div><Text e={Text.Emphasis.Regular}>Hello! (Regular)</Text></div>
<div><Text e={Text.Emphasis.Bold}>Hello! (Bold)</Text></div>
<div><Text e={Text.Emphasis.Light}>Hello! (Light)</Text></div>
<div><Text e={Text.Emphasis.XLight}>Hello! (Extra Light)</Text></div>
<div><Text e={Text.Emphasis.Red}>Hello! (Red)</Text></div>
```

It is also acceptable to use the classes:
```jsx padded
<div className="text-span h1">Hello!</div>
```

Use the monospaced font:
```jsx padded
<div><Text f={Text.Family.Mono}>echo $HELLO</Text></div>
```
