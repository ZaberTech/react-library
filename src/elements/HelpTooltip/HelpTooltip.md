Help provides tooltip upon hover.

```jsx
<div style={{ display: 'flex', alignItems: 'center' }}>
  <HelpTooltip>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
  </HelpTooltip>
</div>
```

It also has a small variant.

```jsx
<div style={{ display: 'flex', alignItems: 'center' }}>
  <HelpTooltip size="small">
    Small help tooltip
  </HelpTooltip>
</div>
```
