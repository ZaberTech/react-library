import React from 'react';
import classNames from 'classnames';

import { Icons } from '../Icons/Icons';
import { PopUp } from '../PopUp/PopUp';

type PopUpProps = React.ComponentPropsWithoutRef<typeof PopUp>;

interface Props extends Omit<PopUpProps, 'dropStyle' | 'size' | 'trigger'> {
  size?: 'large' | 'small';
  trigger?: PopUpProps['trigger'];
}

export const HelpTooltip: React.FC<Props> = ({ className, trigger, size = 'large', ...rest }) => (
  <PopUp
    className={classNames('help-tooltip', className, size)}
    dropStyle="bubble" triggerAction="hover"
    trigger={trigger ?? (({ isOpen }) => <Icons.QuestionMark activated={isOpen}/>)}
    {...rest}/>);
