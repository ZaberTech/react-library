import React, { Component, createRef } from 'react';
import classNames from 'classnames';
import { omit } from '@zaber/toolbox';
import _ from 'lodash';

import { Icons } from '../Icons/Icons';
import { Input } from '../Input/Input';
import { Text, TextType } from '../Text/Text';

import { ConfirmCancel } from './ConfirmCancel';

type InputProps = React.ComponentPropsWithoutRef<typeof Input>;

type ValueType = string | number;
type Validator = (value: string) => boolean;

interface Props extends Omit<InputProps, 'value' | 'onKeyDown' | 'labelContent'> {
  value: ValueType;
  defaultValue?: ValueType;
  validate?: Validator | Validator[];
  onClickComponent?: () => void;
  textType?: TextType;
  editTitle?: string;
  hideButtons?: boolean;
  onEditStateChanged?: (isEditing: boolean) => void;
  labelContent?: React.ReactNode;
}

interface State {
  editing: boolean;
  editedValue: string;
}

export class EditableField extends Component<Props, State> {
  private id: string = _.uniqueId('editable-field');

  private inputRef = createRef<HTMLInputElement>();

  constructor(props: Props) {
    super(props);
    this.state = {
      editedValue: '',
      editing: false,
    };
  }

  static validators = {
    notEmpty: (value: string) => typeof value === 'string' && value !== '',
    notWhitespace: (value: string) => typeof value === 'string' && value.trim().length > 0,
    maxLength: (len: number) => (value: string) => value.length <= len,
    noControl: (value: string) => !/[\u0000-\u001F\u007F-\u009F]/.test(value), // eslint-disable-line no-control-regex
    integer: (value: string) => Number.isInteger(parseInt(value, 10)),
    float: (value: string) => Number.isFinite(parseFloat(value)),
    range: (min: number, max: number) => (value: string) => min <= +value && +value <= max,
  };

  static defaultProps: Pick<Props, 'validate'> = {
    validate: () => true,
  };

  render() {
    const {
      value, disabled, textType, onClickComponent, className,
      labelContent, defaultValue, editTitle, hideButtons, ...rest
    } = this.props;
    const { editing, editedValue } = this.state;
    const isValid = editing && this.isValidInput(editedValue);

    return <div className={classNames('editable-field', className, { 'data-invalid': !isValid && hideButtons })}>
      <label htmlFor={this.id}>{labelContent}</label>

      {editing && <>
        <Input id={this.id}
          value={editedValue}
          onValueChange={this.onValueChanged}
          onKeyDown={this.onKeyDown}
          onBlur={hideButtons ? (isValid ? this.confirm : this.cancel) : undefined}
          ref={this.inputRef}
          {...omit(rest, 'validate', 'onValueChange', 'onEditStateChanged')}/>
        {!hideButtons && <ConfirmCancel
          onConfirm={this.confirm} confirmDisabled={!!disabled || !isValid}
          onCancel={this.cancel} cancelTitle="Cancel Editing"
          onDefault={defaultValue != null ? () => this.onValueChanged(String(defaultValue)) : undefined}
        />}
      </>}
      {!editing && <>
        <Text t={textType} appearsClickable={!!hideButtons || onClickComponent != null} className="value"
          onClick={this.onClickText}>{value}</Text>
        {!hideButtons && <Icons.Edit className="edit" title={editTitle ?? 'Edit Value'} disabled={!!disabled} onClick={this.startEditing}/>}
      </>}
    </div>;
  }

  onClickText = () => {
    const { hideButtons, disabled, onClickComponent } = this.props;

    if (hideButtons && !disabled) {
      this.startEditing();
    }

    onClickComponent?.();
  };

  onKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      const { disabled } = this.props;
      const { editing, editedValue } = this.state;
      const isValid = editing && this.isValidInput(editedValue);
      if (!disabled && isValid) {
        this.confirm();
      }
    } else if (e.key === 'Escape') {
      this.cancel();
    }
  };

  componentDidUpdate(_: Props, prevState: State): void {
    if (this.state.editing && !prevState.editing) {
      const input = this.inputRef.current;
      if (input) {
        input.focus();
        input.select();
      }
    }
  }

  startEditing = () => {
    const { value, onEditStateChanged } = this.props;
    this.setState({ editing: true, editedValue: String(value) });
    onEditStateChanged?.(true);
  };

  onValueChanged = (value: string) => this.setState({ editedValue: value });

  isValidInput = (value: string) => {
    const { validate } = this.props;
    const validators = Array.isArray(validate) ? validate : [validate];
    return validators.every(validator => validator!(value));
  };

  cancel = () => {
    const { onEditStateChanged } = this.props;
    this.setState({ editing: false, editedValue: '' });
    onEditStateChanged?.(false);
  };

  confirm = () => {
    const { onValueChange, onEditStateChanged } = this.props;
    const { editedValue } = this.state;
    this.setState({ editedValue: '', editing: false });
    onValueChange?.(editedValue);
    onEditStateChanged?.(false);
  };
}
