import React from 'react';

import { Button } from '../Button/Button';
import { Icons } from '../Icons/Icons';

interface Props {
  confirmTitle?: string;
  cancelTitle?: string;
  confirmDisabled?: boolean;
  onConfirm: () => void;
  onCancel: () => void;
  onDefault?: () => void;
}

export const ConfirmCancel: React.FC<Props> = ({
  confirmDisabled, confirmTitle = 'Confirm', cancelTitle = 'Cancel',
  onConfirm, onCancel, onDefault,
}) => (
  <div className="confirm-cancel">
    <Icons.Tick title={confirmTitle} className="confirm" disabled={confirmDisabled} onClick={onConfirm}/>
    <div className="spacer"/>
    <Icons.Cross title={cancelTitle} className="cancel" onClick={onCancel}/>
    {!!onDefault && <>
      <div className="spacer"/>
      <Button
        color="clear" size="small"
        onClick={onDefault}>Default Value
      </Button>
    </>}
  </div>
);
