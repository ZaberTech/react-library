A pair of buttons to confirm or cancel edits.

```jsx
<div style={{ width: '300px' }}>
  <ConfirmCancel
    onConfirm={() => console.log('yes!')}
    onCancel={() => console.log('no!')}/>
</div>
```

If the `onDefault` callback is provided, a third button will be added to restore the default value.

Titles can also be provided for the first two buttons.

```jsx
<div style={{ width: '300px' }}>
  <ConfirmCancel
    onConfirm={() => console.log('yes!')}
    onCancel={() => console.log('no!')}
    onDefault={() => console.log('default!')}/>
</div>
```
