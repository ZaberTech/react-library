Standard control for editing a single-line string or number with validation and buttons to apply changes, cancel or revert to default.

```jsx
const [val, setVal] = React.useState('Edit me!');

<div style={{ width: '500px' }}>
  <EditableField labelContent="Label:" value={val} validate={EditableField.validators.notEmpty} onValueChange={setVal}/>
</div>
```

A default value can also be provided, which enables a reset button.


```jsx
const [val, setVal] = React.useState(0);

<div style={{ width: '500px' }}>
  <EditableField value={val} defaultValue={0} validate={EditableField.validators.range(0, 10)} onValueChange={setVal}/>
</div>
```

The buttons can be suppressed, in which case clicking enters edit mode and clicking away counts as a confirmation. The keyboard actions of return for commit and escape for cancel still apply.

The default value property has no effect if the buttons are suppressed.

If validators are provided the border color will change to red, and pressing return will revert to the previous value.


```jsx
const [val, setVal] = React.useState(0);

<div style={{ width: '500px' }}>
  <EditableField value={val} hideButtons={true} validate={EditableField.validators.notEmpty} onValueChange={setVal}/>
</div>
```
