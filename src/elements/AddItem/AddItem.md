Standardized button for adding items to lists or similar actions.

```jsx
const [list, setList] = React.useState('> +');

<div style={{ width: '300px' }}>
  <p>{list}</p>
  <AddItem onClick={() => setList(list + '+')}>Append</AddItem>
</div>
```

