import classNames from 'classnames';
import React from 'react';

import { Button } from '../Button/Button';
import { Icons } from '../Icons/Icons';

type Props = React.ComponentProps<typeof Button>;

export const AddItem: React.FC<Props> = ({
  children,
  className,
  ...buttonProps
}) => (
  <Button className={classNames('add-item', className)} color="clear" {...buttonProps}>
    <div className="add-item-content">
      <Icons.Plus className="add-item-icon"/>
      {children}
    </div>
  </Button>
);
