Class name constant for styling links.

```jsx
import { LinkClasses } from './Link';
  <div>
    <a href='#' className={LinkClasses.Default}>Normal link</a>
    <br/>
    <div className={LinkClasses.Default}>style works with div too</div>
  </div>
```
