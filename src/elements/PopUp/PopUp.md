
Base component for all sort of Pop Ups!

```jsx
<div style={{ display: 'flex', justifyContent: 'space-around', background: 'lightgrey', padding: '2rem' }}>
  <PopUp
    trigger={({ isOpen }) => (<div>Click me ({isOpen ? 'Open' : 'Closed'})!</div>)}>
    I am in pop&nbsp;up! I can display much more text than can be shown in the trigger on its own!
  </PopUp>
  <PopUp
    triggerAction="hover"
    trigger={() => (<div>Hover Here</div>)}>
    I am in pop&nbsp;up too!
  </PopUp>
  <PopUp
    triggerAction="none"
    trigger={() => (<div>Disabled popup</div>)}>
    You won't see this.
  </PopUp>
</div>
```

Pop-ups can appear in a many positions relative to their trigger

```jsx
<div style={{ display: 'flex', justifyContent: 'space-around', background: 'lightgrey', padding: '2rem' }}>
  <PopUp trigger={() => (<div>Upper left</div>)} position="top" align="start">
    Lorem ipsum
  </PopUp>
  <PopUp trigger={() => (<div>Bottom left</div>)} position="bottom" align="start">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </PopUp>
  <PopUp trigger={() => (<div>Upper center</div>)} position="top" align="center">
    Lorem ipsum
  </PopUp>
  <PopUp trigger={() => (<div>Bottom center</div>)} position="bottom" align="center">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </PopUp>
  <PopUp trigger={() => (<div>Upper right</div>)} position="top" align="end">
    Lorem ipsum
  </PopUp>
  <PopUp trigger={() => (<div>Bottom right</div>)} position="bottom" align="end">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </PopUp>
</div>

<div style={{ display: 'flex', justifyContent: 'space-around', background: 'lightgrey', padding: '2rem' }}>
  <PopUp trigger={() => (<div>Left top</div>)} position="left" align="start">
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum
  </PopUp>
  <PopUp trigger={() => (<div>Right top</div>)} position="right" align="start">
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum
  </PopUp>
  <PopUp trigger={() => (<div>Left center</div>)} position="left" align="center">
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum
  </PopUp>
  <PopUp trigger={() => (<div>Right center</div>)} position="right" align="center">
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum
  </PopUp>
  <PopUp trigger={() => (<div>Left bottom</div>)} position="left" align="end">
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum
  </PopUp>
  <PopUp trigger={() => (<div>Right bottom</div>)} position="right" align="end">
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum<br/>
    Lorem ipsum
  </PopUp>
</div>

```

Drop-down style can be changed from bubble to aligned. This is useful for menus.
Also, the default position can be changed.

```jsx
<div style={{ display: 'flex', alignItems: 'center' }}>
  <PopUp dropStyle="aligned" position="top" align="end"
    trigger={({ isOpen }) => (<div>Click me ({isOpen ? 'Open' : 'Closed'})!</div>)}>
    I am in pop up!
  </PopUp>
</div>
```

**WARNING:** *The contents of a pop-up are rendered in the DOM not as a sibling of the trigger, but rather as a new stand-alone element*
*that is a direct decendent of* `body`. *Therefore, any css selectors should not rely on nesting like they would for other components,*
*but instead add any classes that should be used for selection to the* `popupClassName` *property, and nest css under that.*
