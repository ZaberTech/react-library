import React, { useEffect, useLayoutEffect, useRef, useState, DetailedHTMLProps } from 'react';
import classnames from 'classnames';
import { createPortal } from 'react-dom';

import { getZIndex } from '../../misc/utils';

interface TriggerCallbackArg {
  isOpen: boolean;
  setIsOpen: (isOpen: boolean) => void;
}

const ARROW_STYLES = {
  stroke: 'rgba(0, 0, 0, 0.16)',
  strokeWidth: '.25px',
  fill: 'currentcolor',
};

const PopUpArrowHorizontal: React.FC = () => (
  <svg className="pop-up-arrow" viewBox="0 0 20 10">
    <polyline points="0,0 10,10 20,0" {...ARROW_STYLES}/>
  </svg>
);
const PopUpArrowVertical: React.FC = () => (
  <svg className="pop-up-arrow" viewBox="0 0 10 20">
    <polyline points="0,0 10,10 0,20" {...ARROW_STYLES}/>
  </svg>
);

// eslint-disable-next-line @typescript-eslint/no-namespace, import-x/export
export namespace PopUp {
  export type Position = 'top' | 'bottom' | 'left' | 'right';
  export type Align = 'start' | 'end' | 'center';
  export type DropStyle = 'bubble' | 'aligned';
}
interface Props extends DetailedHTMLProps<React.InputHTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  popupClassName?: string;
  dropStyle?: PopUp.DropStyle;
  position?: PopUp.Position;
  align?: PopUp.Align;
  trigger: (info: TriggerCallbackArg) => React.ReactNode;
  triggerAction?: 'click' | 'hover' | 'none';
  onOpen?: () => void;
  onClose?: () => void;
  /** If true, the popup will close when the view is clicked. Defaults to true. */
  closeOnClick?: boolean;
}


// eslint-disable-next-line import-x/export
export const PopUp: React.FC<Props> = props => {
  const {
    dropStyle = 'bubble',
    triggerAction = 'click',
    position = 'bottom',
    align = 'start',
    className,
    popupClassName,
    closeOnClick,
    trigger,
    children,
    ...rest
  } = props;

  const [isOpen, setIsOpen] = useState(false);

  const triggerRef = useRef<HTMLDivElement>(null);

  return (
    <div className={classnames('pop-up', className)} {...rest}>
      <div ref={triggerRef}
        className={classnames({ 'pop-up-trigger': triggerAction !== 'none' })}
        onClick={triggerAction === 'click' ? () => setIsOpen(true) : undefined}
        onMouseEnter={triggerAction === 'hover' ? () => setIsOpen(true) : undefined}
        onMouseLeave={triggerAction === 'hover' ? () => setIsOpen(false) : undefined}
      >{trigger({ isOpen, setIsOpen })}</div>
      {isOpen && createPortal(
        <PopUpView
          {...{ position, align, dropStyle, popupClassName, closeOnClick }}
          close={triggerAction === 'click' ? () => setIsOpen(false) : undefined}
          triggerRef={triggerRef}
        >
          {children}
        </PopUpView>,
        document.body,
      )}
    </div>
  );
};

interface Placement {
  top: number;
  left: number;
}

function isSpaceAbove(triggerBox: DOMRect, contentBox: DOMRect): boolean {
  return triggerBox.top > contentBox.height;
}

function isSpaceBelow(triggerBox: DOMRect, contentBox: DOMRect, windowHeight: number): boolean {
  return (windowHeight - triggerBox.bottom) > contentBox.height;
}

function isSpaceLeft(triggerBox: DOMRect, contentBox: DOMRect): boolean {
  return triggerBox.left > contentBox.width;
}

function isSpaceRight(triggerBox: DOMRect, contentBox: DOMRect, windowWidth: number): boolean {
  return (windowWidth - triggerBox.right) > contentBox.width;
}

/** Finds the best position for this pop-up using the user requested position as the default */
function findPosition(position: PopUp.Position, triggerBox?: DOMRect, contentBox?: DOMRect): PopUp.Position {
  const { clientWidth, clientHeight } = document.documentElement;
  if (!triggerBox || !contentBox) {
    return position;
  }

  switch (position) {
    case 'top':
      if (!isSpaceAbove(triggerBox, contentBox) && isSpaceBelow(triggerBox, contentBox, clientHeight)) {
        return 'bottom';
      }
      return 'top';
    case 'bottom':
      if (!isSpaceBelow(triggerBox, contentBox, clientHeight) && isSpaceAbove(triggerBox, contentBox)) {
        return 'top';
      }
      return 'bottom';
    case 'left':
      if (!isSpaceLeft(triggerBox, contentBox) && isSpaceRight(triggerBox, contentBox, clientWidth)) {
        return 'right';
      }
      return 'left';
    case 'right':
      if (!isSpaceRight(triggerBox, contentBox, clientWidth) && isSpaceLeft(triggerBox, contentBox)) {
        return 'left';
      }
      return 'right';
    default:
      return position;
  }
}

interface Anchors {
  start: number;
  center: number;
  end: number;
}

/** Determines if the content can be aligned in a given client window. */
class CanAlign {
  readonly sizeDimension = this.dimension === 'horizontal' ? 'width' : 'height';
  private readonly anchors: Anchors;

  constructor(
    readonly dimension: 'horizontal' | 'vertical',
    readonly client: { width: number; height: number },
    readonly contentBox: { width: number; height: number },
    triggerBox: DOMRect,
    dropStyle: PopUp.DropStyle,
  ) {
    const start = dimension === 'horizontal' ? 'left' : 'top';
    const end = dimension === 'horizontal' ? 'right' : 'bottom';
    const center = triggerBox[start] + triggerBox[this.sizeDimension] / 2;
    this.anchors = {
      start: dropStyle === 'aligned' ? triggerBox[start] : center,
      center,
      end: dropStyle === 'aligned' ? triggerBox[end] : center,
    };
  }

  toStart(): boolean {
    return this.client[this.sizeDimension] - this.anchors.start > this.contentBox[this.sizeDimension];
  }

  toCenter(): boolean {
    return this.anchors.center > this.contentBox[this.sizeDimension] / 2
      && this.client[this.sizeDimension] - this.anchors.center > this.contentBox[this.sizeDimension] / 2;
  }

  toEnd(): boolean {
    return this.anchors.end > this.contentBox[this.sizeDimension];
  }
}

function findAlignment(
  dimension: 'horizontal' | 'vertical',
  align: PopUp.Align,
  dropStyle: PopUp.DropStyle,
  triggerBox?: DOMRect,
  contentBox?: DOMRect,
): PopUp.Align {
  if (!triggerBox || !contentBox) {
    return align;
  }

  const client = { width: document.documentElement.clientWidth, height: document.documentElement.clientHeight };
  const canAlign = new CanAlign(dimension, client, contentBox, triggerBox, dropStyle);

  switch (align) {
    case 'start':
      if (!canAlign.toStart()) {
        if (canAlign.toCenter()) {
          return 'center';
        } else if (canAlign.toEnd()) {
          return 'end';
        }
      }
      break;
    case 'end':
      if (!canAlign.toEnd()) {
        if (canAlign.toCenter()) {
          return 'center';
        } else if (canAlign.toStart()) {
          return 'start';
        }
      }
      break;
    case 'center':
      if (!canAlign.toCenter()) {
        if (canAlign.toStart()) {
          return 'start';
        } else if (canAlign.toEnd()) {
          return 'end';
        }
      }
      break;
  }

  return align;
}

type ViewProps = {
  position: NonNullable<Props['position']>;
  align: NonNullable<Props['align']>;
  dropStyle: NonNullable<Props['dropStyle']>;
  popupClassName: Props['popupClassName'];
  /** A function to use to close the popup when an overlay is clicked. If undefined, do not use the overlay */
  close?: () => void;
  /** If true, the popup will close when the view is clicked. Defaults to true. */
  closeOnClick?: boolean;
  triggerRef : React.RefObject<HTMLDivElement>;
};

const PopUpView: React.FC<ViewProps> = props => {
  const {
    position: targetPosition, align: targetAlign, dropStyle, popupClassName, close, triggerRef, children,
    closeOnClick = true,
  } = props;
  const popUpRef = useRef<HTMLDivElement>(null);

  const [targetX, setTargetX] = useState(0);
  const [targetY, setTargetY] = useState(0);
  const [targetWidth, setTargetWidth] = useState(0);
  const [targetHeight, setTargetHeight] = useState(0);

  const [placement, setPlacement] = useState<Placement>({ top: 0, left: 0 });
  const [zIndex, setZIndex] = useState(1);

  useEffect(() => {
    let animationFrameId: number;
    const checkTarget = () => {
      const box = triggerRef.current?.getBoundingClientRect();
      if (box != null) {
        setTargetX(box.x);
        setTargetY(box.y);
        setTargetWidth(box.width);
        setTargetHeight(box.height);
      }
      animationFrameId = requestAnimationFrame(checkTarget);
    };
    animationFrameId = requestAnimationFrame(checkTarget);

    return () => window.cancelAnimationFrame(animationFrameId);
  }, []);

  const contentBox = popUpRef.current?.getBoundingClientRect();
  const triggerBox = triggerRef.current?.getBoundingClientRect();

  const position = findPosition(targetPosition, triggerBox, contentBox);
  const isHorizontal = position === 'top' || position === 'bottom';
  const align = findAlignment(isHorizontal ? 'horizontal' : 'vertical', targetAlign, dropStyle, triggerBox, contentBox);

  useLayoutEffect(
    () => {
      if (!contentBox || !triggerBox) {
        setPlacement({ top: 0, left: 0 });
        return;
      }

      setZIndex(getZIndex(triggerRef.current) + 1);

      if (position === 'top' || position === 'bottom') {
        const top = (position === 'top' ? targetY - contentBox.height : targetY + targetHeight);
        const triggerCenter = targetX + targetWidth / 2;
        if (align === 'center') {
          setPlacement({ top, left: triggerCenter - contentBox.width / 2 });
        }
        if (align === 'start') {
          if (dropStyle === 'bubble') {
            setPlacement({ top, left: triggerCenter });
          } else {
            setPlacement({ top, left: targetX });
          }
        }
        if (align === 'end') {
          if (dropStyle === 'bubble') {
            setPlacement({ top, left: triggerCenter - contentBox.width });
          } else {
            setPlacement({ top, left: targetX + targetWidth - contentBox.width });
          }
        }
      } else {
        const left = (position === 'left' ? targetX - contentBox.width : targetX + targetWidth);
        const triggerCenter = targetY + targetHeight / 2;
        if (align === 'center') {
          setPlacement({ top: triggerCenter - contentBox.height / 2, left });
        }
        if (align === 'start') {
          if (dropStyle === 'bubble') {
            setPlacement({ top: triggerCenter, left });
          } else {
            setPlacement({ top: targetY, left });
          }
        }
        if (align === 'end') {
          if (dropStyle === 'bubble') {
            setPlacement({ top: triggerCenter - contentBox.height, left });
          } else {
            setPlacement({ top: targetY + targetHeight - contentBox.height, left });
          }
        }
      }
    },
    [
      position, align, targetX, targetY, targetWidth, targetHeight,
      contentBox?.width, contentBox?.height, triggerBox?.top, triggerBox?.bottom
    ]
  );

  const filterClose = (e: React.MouseEvent) => {
    if (e.isDefaultPrevented()) {
      return;
    }
    close?.();
  };

  return (
    <div className={classnames('popped-up', popupClassName, close == null && 'transparent')}
      onClick={closeOnClick ? filterClose : undefined}
      style={{ zIndex }}>
      {close != null && <div className="pop-up-overlay" onClick={close}/>}
      <div ref={popUpRef} className={classnames('pop-up-view', position, align, dropStyle)} style={placement}>
        <div className="pop-up-content" onClick={closeOnClick ? filterClose : undefined}>
          {children}
        </div>
        {dropStyle === 'bubble' && (isHorizontal ? <PopUpArrowHorizontal/> : <PopUpArrowVertical/>)}
      </div>
    </div>);
};
