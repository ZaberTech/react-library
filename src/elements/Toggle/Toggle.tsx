import React, { Component } from 'react';
import classNames from 'classnames';
import { omit } from '@zaber/toolbox';

import ToggleIcon from '../../../assets/img/toggle.svg';

interface Props extends Omit<React.DetailedHTMLProps<React.HTMLAttributes<HTMLOrSVGElement>, HTMLOrSVGElement>, 'value'> {
  value?: boolean;
  loading?: boolean;
  onValueChange?: (value: boolean) => void;
  disabled?: boolean;
}

export class Toggle extends Component<Props> {
  onToggle = (e: React.MouseEvent<HTMLOrSVGElement>) => {
    const { value, onValueChange, disabled, loading, onClick } = this.props;
    if (onValueChange && !disabled && !loading) {
      onValueChange(!value);
    }
    if (onClick) {
      onClick(e);
    }
  };

  render(): React.ReactNode {
    const { className, value, loading, disabled, ...rest } = this.props;
    // Ref gets omitted from the type since it's strangely incompatible.
    const passedProps = omit(rest as Omit<typeof rest, 'ref'>, 'onValueChange', 'onClick');
    return (
      <ToggleIcon
        className={classNames('toggle-button', className, { 'toggle-on': value, 'toggle-off': !value, disabled, loading })}
        onClick={this.onToggle}
        role="checkbox"
        aria-disabled={disabled}
        aria-checked={value}
        {...passedProps}/>
    );
  }
}
