Set `value` to toggle it on or off. Provide `onValueChange` callback to learn when user toggles the button.

```jsx
const [value, setValue] = React.useState(true);
<div style={{ fontSize: '2rem' }}>
  <Toggle
    value={value}
    onValueChange={setValue}/>&nbsp;Click!<br/>
  <Toggle
    value={!value}
    onValueChange={setValue}/><br/>
</div>
```

You can disable it.

```jsx
<div style={{ fontSize: '2rem' }}>
  <Toggle
    value={true}
    disabled/><br/>
</div>
```

You can use `loading` property to display intermediate state if there is an asynchronous operation.
The `onValueChange` callback won't trigger during the loading.

```jsx
<div style={{ fontSize: '2rem' }}>
  <Toggle
    value={true}
    loading/><br/>
  <Toggle
    value={false}
    loading/><br/>
</div>
```
