The `NumericInputEditor` is both a usable component, and also a reference implementation for anyone
who wants to create their own input editor. It is useful if you have a numeric value that should be written
to a device asynchronously.

These components are made to work with underlying values that are changed asynchronously. To simulate this,
the component will remain in the initialization mode until you click the complete button, and in the write mode
for 1 second before switching to display.

```jsx
import { NumericInputEditor } from './NumericInputEditor';
import { Flex } from '../../layout/Flex/Flex';

const FakedNumericInputEditor = ({ label, align }) => {
  const [mode, setMode] = React.useState('initialize');
  const [value, setValue] = React.useState(null);

  return <Flex.Column style={{ gap: '1rem' }}>
    <NumericInputEditor
      labelContent={label}
      value={value}
      mode={mode}
      defaultValue={0}
      onChange={({value, mode}) => {
        setValue(value);
        setMode(mode);
        if (mode === 'write') {
          setTimeout(() => setMode('display'), 1000);
        }
      }}
      align={align}
    />
    { mode === 'initialize' && <button onClick={() => { setValue(0); setMode('display')}}>Complete Initialization</button>}
  </Flex.Column>;
}

<Flex.Row style={{ alignItems: 'flex-start', gap: '1rem' }}>
  <FakedNumericInputEditor label="Start Aligned" align="start"/>
  <FakedNumericInputEditor label="Center Aligned" align="center"/>
  <FakedNumericInputEditor label="End Aligned" align="end"/>
</Flex.Row>
```
