import classnames from 'classnames';
import React from 'react';

import { NumericInput } from '../Input/NumericInput';

import { InputEditor } from './InputEditor';

type InputEditorType = InputEditor<number | null, number>;

interface Props extends Omit<React.ComponentPropsWithoutRef<typeof NumericInput>, 'onChange' | 'onNumberChange' | 'defaultValue'> {
  value: number | null;
  mode: InputEditorType['props']['mode'];
  onChange: InputEditorType['props']['onChange'];
  align?: InputEditorType['props']['align'];
  isValid?: (value: number) => boolean;
  renderExtra?: InputEditorType['props']['renderExtra'];
  defaultValue?: InputEditorType['props']['defaultValue'];
}

export const NumericInputEditor: React.FC<Props> = props => {
  const {
    value, mode, onChange, renderExtra, defaultValue, disabled, isValid = () => true, align = 'start', id, className, ...rest
  } = props;
  const isDisabled = disabled ? disabled : mode === 'initialize' || mode === 'write';

  return (
    <InputEditor<number | null, number>
      value={value}
      isValid={(value): value is number => value != null && !isNaN(value) && isValid(value)}
      mode={mode}
      defaultValue={defaultValue}
      onChange={onChange}
      className={classnames('numeric-input-editor', className)}
      renderExtra={renderExtra}
      renderInput={({ ref, setValue, startEditing, placeholder }) => (
        <NumericInput
          {...rest}
          id={placeholder ? undefined : id}
          ref={ref}
          value={value}
          onNumberChange={setValue}
          disabled={isDisabled}
          onFocus={startEditing}
        />
      )}
      align={align}
    />
  );
};
