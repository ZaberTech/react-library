The `InputEditor` is an abstract component that is designed to help building other components.
A reference implementation can be found in `NumericInputEditor`.

This component has a few different modes it can be in:
* `'initialize'`: The mode to use while waiting for the value to be first fetched
* `'display'`: Displays a value
* `'edit'`: Allows a user to change the value without sending it to the device yet
* `'write'`: A state for when some asynchronous action is occurring. It will display the value, but not allow it to be changed.

This component also does some front end input validation. It will never call `onChange` with the `'write'` mode
unless the input is valid. Validation is managed by the `isValid` function, that should return a boolean
specifying if the value is valid. Further, if the `isValid` function is a typeguard, it will narrow the input type
accordingly, allowing any code to be confident that `onChange` will always return the narrowed type for writing.
See `NumericInputEditor` for how to ensure that the value returned on `'write'` is always a valid number.


**A note on testing:** This component will render the input twice when the pop-up is open, one to determine where on the page the pop-up
should be positioned, and one contained in the pop-up. The first is only used for layout and has `visibility: hidden` so users will not
interact with it. When testing this app, you should always use jest's `getByRole` function to get the input since it will ignore hidden
elements, unlike the other `get*` functions. The role's `name` should be the input's label if possible, or `aria-label` if you can't use
a label for some reason.