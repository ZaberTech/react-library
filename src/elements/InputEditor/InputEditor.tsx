import React, { Component, createRef } from 'react';
import { createPortal } from 'react-dom';
import classnames from 'classnames';
import _ from 'lodash';

import { PopUp } from '../PopUp/PopUp';
import { ButtonRowConfirmCancel } from '../ButtonRow/ButtonRowConfirmCancel';
import { ButtonOnClick } from '../Button/Button';
import { Icons } from '../Icons/Icons';
import { Text } from '../Text/Text';
import { environment } from '../../environment';

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace InputEditor {
  export type Mode = 'initialize' | 'display' | 'edit' | 'write';

  export type State<InputType, ValidatedType extends InputType> = {
    mode: 'initialize' | 'display' | 'edit';
    value: InputType;
  } | {
    mode: 'write';
    value: ValidatedType;
  };
}

interface SubcomponentProps<InputType> {
  mode: InputEditor.Mode;
  value: InputType;
  setValue: (value: InputType) => void;
  startEditing: () => void;
}

interface InputProps<InputType> extends SubcomponentProps<InputType> {
 ref: React.RefObject<HTMLInputElement>;
 /** Lets this component know if it's the real component, or just a space-taking placeholder. */
 placeholder: boolean;
}

type DivProps = React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>;
interface Props<InputType, ValidatedType extends InputType> extends Omit<DivProps, 'onChange' | 'defaultValue'> {
  value: InputType;
  mode: InputEditor.Mode;
  isValid: (value: InputType) => value is ValidatedType;
  onChange: (result: InputEditor.State<InputType, ValidatedType>) => void;
  /** A function to render the input, it will appear in all modes. */
  renderInput: (props: InputProps<InputType>) => React.ReactNode;
  /** A component to render when the editor is in `edit` mode, allowing for custom controls. */
  renderExtra?: (props: SubcomponentProps<InputType>) => React.ReactNode;
  align: PopUp.Align;
  defaultValue?: ValidatedType | null;
}

type State<InputValue> = {
  preEditValue: InputValue;
  open: boolean;
};

export class InputEditor<Input, Validated extends Input> extends Component<Props<Input, Validated>, State<Input>> {
  private inputRef: React.RefObject<HTMLInputElement>;
  private hiddenInputRef: React.RefObject<HTMLInputElement>;
  private anchorRef: React.RefObject<HTMLDivElement>;

  static defaultProps = { align: 'start' };

  constructor(props: Props<Input, Validated>) {
    super(props);
    this.inputRef = createRef();
    this.hiddenInputRef = createRef();
    this.anchorRef = createRef();
    this.state = {
      preEditValue: props.value,
      open: props.mode === 'edit',
    };
  }

  componentDidMount() {
    window.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown);
  }

  handleKeyDown = (e: KeyboardEvent) => {
    if (this.props.mode !== 'edit') { return }
    if (e.key === 'Enter' && this.props.isValid(this.props.value)) {
      e.preventDefault();
      this.props.onChange({ mode: 'write', value: this.props.value });
    } else if (e.key === 'Escape') {
      e.preventDefault();
      this.props.onChange({ mode: 'display', value: this.state.preEditValue });
    }
  };

  startEditing = () => {
    if (this.props.mode === 'display') {
      this.setState({ open: true, preEditValue: this.props.value });
      this.props.onChange({ mode: 'edit', value: this.props.value });
    }
  };

  render() {
    const { value, isValid, mode, renderInput, renderExtra, onChange, align, className, defaultValue, ...rest } = this.props;
    const { open, preEditValue } = this.state;
    const setValue = mode === 'display' || mode === 'edit' ? (value: Input) => onChange({ mode, value }) : () => null;
    const classNames = classnames('input-editor', mode, className);
    if (open) {
      return <>
        <div className={classNames} ref={this.anchorRef} style={{ visibility: 'hidden' }}>
          {renderInput({ mode, value, setValue: () => null, startEditing: () => null, ref: this.hiddenInputRef, placeholder: true })}
        </div>
        {createPortal(
          <PopOut
            align={align}
            className={classNames}
            anchorRef={this.anchorRef}
            mode={mode}
            input={renderInput({ mode, value, setValue, startEditing: this.startEditing, ref: this.inputRef, placeholder: false })}
            inputRef={this.inputRef}
            extra={renderExtra?.({ mode, value, setValue, startEditing: this.startEditing })}
            onConfirm={isValid(value) ? () => onChange({ mode: 'write', value }) : 'disabled'}
            onCancel={() => onChange({ mode: 'display', value: preEditValue })}
            setDefaultValue={defaultValue != null ? () => onChange({ mode: 'edit', value: defaultValue }) : null}
            onClose={() => this.setState({ open: false })}
          />,
          document.body
        )}
      </>;
    } else {
      return <div className={classNames} {...rest}>
        {renderInput({ mode, value, setValue, startEditing: this.startEditing, ref: this.inputRef, placeholder: false })}
      </div>;
    }
  }
}

type PopOutProps = {
  align: PopUp.Align;
  className: string;
  anchorRef: React.RefObject<HTMLDivElement>;
  mode: InputEditor.Mode;
  input: React.ReactNode;
  inputRef: React.RefObject<HTMLInputElement>;
  extra?: React.ReactNode;
  onConfirm: ButtonOnClick;
  onCancel: () => void;
  setDefaultValue?: (() => void) | null;
  /** A function that is called once this pop out has finished animating itself closed */
  onClose: () => void;
};

type PopOutState = {
  open: boolean;
  animating: boolean;
  placement: { top: number; left: number; width: number } | null;
};

class PopOut extends Component<PopOutProps, PopOutState> {
  private animationFrameId = 0;
  private popupRef = createRef<HTMLDivElement>();

  constructor(props: PopOutProps) {
    super(props);
    this.state = {
      open: false,
      animating: false,
      placement: null,
    };
  }

  setPlacement = () => {
    const anchorBox = this.props.anchorRef.current?.getBoundingClientRect();
    const clientWidth = document.documentElement.clientWidth;
    if (anchorBox) {
      const { align } = this.props;
      const top = anchorBox.top;
      const anchorCenter = anchorBox.left + anchorBox.width / 2;
      const width =
        align === 'start' ? clientWidth - anchorBox.left :
        align === 'end' ? anchorBox.right :
        Math.min(anchorCenter, clientWidth - anchorCenter) * 2;
      const left =
        align === 'start' ? anchorBox.left :
        align === 'end' ? 0 :
        anchorCenter - width / 2;
      this.setState({ placement: { top, left, width } });
    }
    this.animationFrameId = requestAnimationFrame(this.setPlacement);
  };

  clickOutsideToCancel = (e: MouseEvent) => {
    if (this.popupRef.current == null || this.state.animating || this.state.placement == null) {
      return;
    }
    if (this.props.mode !== 'edit') {
      e.preventDefault();
      e.stopPropagation();
      return;
    }
    const box = this.popupRef.current.getBoundingClientRect();
    if (e.clientX < box.left || e.clientX > box.right || e.clientY < box.top || e.clientY > box.bottom) {
      this.props.onCancel();
    }
  };

  componentDidMount() {
    if (environment.isTest) {
      this.setState({ placement: { top: 0, left: 0, width: 0 } });
    } else {
      this.animationFrameId = requestAnimationFrame(this.setPlacement);
      window.addEventListener('click', this.clickOutsideToCancel, true);
    }
  }

  componentWillUnmount() {
    if (!environment.isTest) {
      cancelAnimationFrame(this.animationFrameId);
      window.removeEventListener('click', this.clickOutsideToCancel, true);
    }
  }

  componentDidUpdate(_prevProps: PopOutProps, prevState: PopOutState): void {
    if (prevState.placement == null && this.state.placement != null) {
      this.props.inputRef.current?.focus({ preventScroll: true });
      this.props.inputRef.current?.select();
    }
    if (this.state.animating) {
      return;
    }
    if (!this.state.open && this.props.mode === 'edit') {
      this.animate(true);
    }
    const closedMode = this.props.mode === 'display' || this.props.mode === 'initialize';
    if (this.state.open && closedMode) {
      this.animate(false);
    }
  }

  animate = (open: boolean) => {
    if (environment.isTest) {
      this.finishAnimation(open);
      return;
    }
    if (this.props.anchorRef.current == null || this.popupRef.current == null) {
      return;
    }
    this.setState({ animating: true });
    const expand = this.props.mode === 'edit';
    const anchorBox = this.props.anchorRef.current.getBoundingClientRect();
    const popupBox = this.popupRef.current.getBoundingClientRect();
    const collapsedStyles = {
      height: `${anchorBox.height}px`,
      width: `${anchorBox.width}px`,
      padding: '0',
      transform: 'translate(0, 0)',
      border: 'none',
      boxShadow: 'none',
    };
    const expandedStyles: typeof collapsedStyles = {
      height: `${popupBox.height}px`,
      width: `${popupBox.width}px`,
      ..._.pick(this.popupRef.current.style, 'padding', 'transform', 'border', 'boxShadow'),
    };
    const fromStyles = expand ? collapsedStyles : expandedStyles;
    const toStyles = expand ? expandedStyles : collapsedStyles;
    const animation = this.popupRef.current.animate(
      [fromStyles, toStyles],
      { duration: 250, easing: 'ease-in-out' }
    );
    animation.onfinish = () => this.finishAnimation(open);
  };

  finishAnimation = (open: boolean) => {
    this.setState({ animating: false, open });
    if (!open) {
      this.props.onClose();
    }
  };

  render(): React.ReactNode {
    const { placement } = this.state;
    if (placement == null) {
      return null;
    }
    const { input, extra, mode, setDefaultValue, onConfirm, onCancel, align, className } = this.props;
    return <div className="input-popup-backdrop" onClick={mode === 'edit' ? () => onCancel() : undefined}>
      <div className={classnames('popup-positioner', align)} style={placement}>
        <div className={classnames(className, 'editing-popup')} ref={this.popupRef} onClick={e => e.stopPropagation()}>
          <div>{input}</div>
          {extra && <div>{extra}</div>}
          {setDefaultValue && <div className="restore-default" onClick={setDefaultValue}><Icons.Restore/><Text>Default Value</Text></div>}
          <ButtonRowConfirmCancel confirmText="Save" onConfirm={mode === 'edit' ? onConfirm : 'disabled'} onCancel={onCancel}/>
        </div>
      </div>
    </div>;
  }
}
