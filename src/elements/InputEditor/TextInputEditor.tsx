import classnames from 'classnames';
import React from 'react';
import { omit } from '@zaber/toolbox';

import { Input } from '../Input/Input';

import { InputEditor } from './InputEditor';

type InputProps = React.ComponentPropsWithRef<typeof Input>;
type InputEditorType = InputEditor<string, string>;

interface Props extends Omit<InputProps, 'onChange' | 'onValueChange' | 'defaultValue'> {
  value: InputEditorType['props']['value'];
  mode: InputEditorType['props']['mode'];
  onChange: InputEditorType['props']['onChange'];
  align?: InputEditorType['props']['align'];
  isValid?: (value: string) => boolean;
  defaultValue?: InputEditorType['props']['defaultValue'];
  renderExtra?: InputEditorType['props']['renderExtra'];
}

export const TextInputEditor: React.FC<Props> = props => {
  const {
    value, mode, onChange, isValid = () => true, align = 'start', id, renderExtra, disabled, className, ...rest
  } = props;
  const isDisabled = disabled ? disabled : mode === 'initialize' || mode === 'write';

  return (
    <InputEditor<string, string>
      value={value}
      isValid={(value): value is string => value != null && isValid(value)}
      mode={mode}
      onChange={onChange}
      className={classnames('text-input-editor', className)}
      renderExtra={renderExtra}
      renderInput={({ ref, setValue, startEditing, placeholder }) => (
        <Input
          {...omit(rest, 'defaultValue')}
          id={placeholder ? undefined : id}
          ref={ref}
          value={value}
          onChange={value => setValue(value.target.value)}
          disabled={isDisabled}
          onFocus={startEditing}
        />
      )}
      align={align}
    />
  );
};
