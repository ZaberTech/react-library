Another pop-out editor used to edit text values (like vanilla `Input`)

```jsx
import { TextInputEditor } from './TextInputEditor';
import { Flex } from '../../layout/Flex/Flex';

const FakedTextInputEditor = ({ label, align }) => {
  const [mode, setMode] = React.useState('initialize');
  const [value, setValue] = React.useState('');

  return <div>
    <TextInputEditor
      labelContent={label}
      value={value}
      mode={mode}
      onChange={({value, mode}) => {
        setValue(value);
        setMode(mode);
        if (mode === 'write') {
          setTimeout(() => setMode('display'), 1000);
        }
      }}
      align={align}
    />
    {mode === 'initialize' && (
      <button style={{ marginTop: '1rem' }} onClick={() => { setValue(''); setMode('display')}}>Complete Initialization</button>
    )}
  </div>;
}

<Flex.Row style={{ alignItems: 'flex-start', gap: '1rem' }}>
  <FakedTextInputEditor label="Start Aligned" align="start"/>
  <FakedTextInputEditor label="Center Aligned" align="center"/>
  <FakedTextInputEditor label="End Aligned" align="end"/>
</Flex.Row>
```
