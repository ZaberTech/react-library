import React, { Component } from 'react';
import classNames from 'classnames';

type DivProps = React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLDivElement>, HTMLDivElement>;

interface MinorMenuProps extends DivProps {
  barStyle?: 'horizontal' | 'vertical';
}

const Item: React.FunctionComponent<DivProps> = ({ children, className, ...props }) =>
  (<div className={classNames('minor-menu-item', className)} {...props}>
    <div className="content">
      {children}
    </div>
  </div>);

export class MinorMenu extends Component<MinorMenuProps> {
  static Item = Item;

  render(): React.ReactNode {
    const { className, barStyle = 'vertical', ...rest } = this.props;
    return <div className={classNames('minor-menu', className, barStyle)} {...rest}/>;
  }
}
