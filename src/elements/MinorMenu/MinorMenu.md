Secondary menu. It is designed to be used with React Router NavLink or a simple `a` tag.
The `active` class on wrapping element triggers the strip and bold text. By default the
menu is vertically stacked with a bar displayed at the side for the active item.
(Optionally put `barStyle='vertical'` as a prop)

```jsx
<div style={{ display: 'flex', alignItems: 'center' }}>
  <MinorMenu>
    <a href="#"><MinorMenu.Item>Item 1</MinorMenu.Item></a>
    <a href="#" className="active"><MinorMenu.Item>Item 2</MinorMenu.Item></a>
    <a href="#"><MinorMenu.Item>Item 3</MinorMenu.Item></a>
    <a href="#" className="disabled"><MinorMenu.Item>Disabled</MinorMenu.Item></a>
  </MinorMenu>
</div>
```

There is also a horizontally stacked version with active bar at the bottom.

```jsx
<div style={{ display: 'flex', alignItems: 'center' }}>
  <MinorMenu barStyle='horizontal'>
    <a href="#"><MinorMenu.Item>Item 1</MinorMenu.Item></a>
    <a href="#" className="active"><MinorMenu.Item>Item 2</MinorMenu.Item></a>
    <a href="#"><MinorMenu.Item>Item 3</MinorMenu.Item></a>
  </MinorMenu>
</div>
```
