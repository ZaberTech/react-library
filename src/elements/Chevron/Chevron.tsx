import React from 'react';
import classNames from 'classnames';

import { Icons } from '../Icons/Icons';
import { SvgIconProps } from '../SvgIcon/SvgIcon';

interface Props extends SvgIconProps {
  expanded: boolean;
  titleCollapsed?: string;
  titleExpanded?: string;
}

export const Chevron: React.FC<Props> = ({ className, expanded, title, titleCollapsed, titleExpanded, ...rest }) => {
  const commonProps: SvgIconProps = {
    className: classNames('chevron', className, { expanded, collapsed: !expanded }),
    activated: expanded,
    appearsClickable: true,
    ...rest,
  };
  return (<>
    {!!expanded && <Icons.ArrowExpanded title={titleExpanded ?? title} {...commonProps}/>}
    {!expanded && <Icons.ArrowCollapsed title={titleCollapsed ?? title} {...commonProps}/>}
  </>);
};
