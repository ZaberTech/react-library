Simple example:

```jsx
const [expanded, setExpanded] = React.useState(false);
return <Chevron style={{ fontSize: '2.5rem' }} expanded={expanded} onClick={() => setExpanded(!expanded)}/>
```
