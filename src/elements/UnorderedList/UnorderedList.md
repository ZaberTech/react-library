Simple example:

```jsx
import { Text } from '../Text/Text';

<UnorderedList
  header={<Text t={Text.Type.H4}>An unordered list of strings:</Text>}
>{[
  'First Item',
  'Second Item',
  'Third Item',
]}</UnorderedList>
```

Or if you need more control over how the lines looks, you can provide the children as normal. Each top level child component gets a bullet:

```jsx
import { Text } from '../Text/Text';

<UnorderedList
  header={<Text t={Text.Type.H4}>An unordered list of strings with formatting:</Text>}
>
  <Text>An item in the list</Text>
  <Text e={Text.Emphasis.Bold}>Another item</Text>
</UnorderedList>
```
