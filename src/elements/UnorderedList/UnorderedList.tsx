import classNames from 'classnames';
import React, { Children } from 'react';

type DivProps = React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLDivElement>, HTMLDivElement>;

interface Props extends DivProps {
  header: React.ReactNode;
}

export const UnorderedList: React.FC<Props> = ({ className, header, children, ...rest }) => (
  <div className={classNames('unordered-list', className)} {...rest}>
    <div className="unordered-list-header">{header}</div>
    <div className="unordered-list-items">
      {Children.map(children, (child, i) => (<div key={i} className="unordered-list-item">
        <svg viewBox="0 0 16 16" className="unordered-list-bullet">
          <circle style={{ fill: 'currentColor' }} cx="8" cy="8" r="8"/>
        </svg>
        {child}
      </div>))}
    </div>
  </div>
);
