Context Menu displays a kebab icon that opens a menu upon clicking.

```jsx
import { Icons } from '../Icons/Icons';
import { Checkbox } from '../Input/Checkbox';

import _ from 'lodash';

const [checked, setChecked] = React.useState(false);

<div style={{ display: 'flex', alignItems: 'center' }}>
  Menu here&emsp;
  <ContextMenu>
    <ContextMenu.Item onClick={_.noop} icon={<Icons.NewWindow/>}>Open in a New Window</ContextMenu.Item>
    <ContextMenu.Item onClick={_.noop} icon={<Icons.Refresh/>}>Refresh</ContextMenu.Item>
    <ContextMenu.Item onClick={_.noop} disabled icon={<Icons.Trash/>}>Disabled</ContextMenu.Item>
    <ContextMenu.CheckboxItem checked={checked} onChecked={setChecked}>
      Checkbox Item
    </ContextMenu.CheckboxItem>
    <ContextMenu.Item>No action</ContextMenu.Item>
  </ContextMenu>
</div>
```

Use `position` and `align` to change where the menu appears by default.

```jsx
<div style={{ display: 'flex', alignItems: 'center' }}>
  Menu Here&emsp;
  <ContextMenu position="top" align="end">
    <ContextMenu.Item>No action</ContextMenu.Item>
  </ContextMenu>
</div>
```

You can set the `dropStyle=aligned` to make the menu aligned and without the "bubble tip".
Use `trigger` to set custom trigger (replacing the kebab icon).

```jsx
<div style={{ display: 'flex', alignItems: 'center' }}>
  <ContextMenu dropStyle="aligned"
    trigger={({ isOpen }) => (<div>Click me ({isOpen ? 'Open' : 'Closed'})!</div>)}>
    <ContextMenu.Item>Nothing is happening here</ContextMenu.Item>
  </ContextMenu>
</div>
```
