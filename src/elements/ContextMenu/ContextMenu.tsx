import React, { Component } from 'react';
import classNames from 'classnames';

import { Icons } from '../Icons/Icons';
import { PopUp } from '../PopUp/PopUp';
import { Checkbox } from '../Input/Checkbox';

type DivProps = React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLDivElement>, HTMLDivElement>;

interface ItemProps extends DivProps {
  icon?: React.ReactNode;
}

export const Item: React.FC<ItemProps>  = ({ className, disabled, onClick, children, icon, ...rest }) => (
  <div
    className={classNames('context-menu-item', className, { 'clickable': !!onClick, disabled, 'has-icon': !!icon })}
    role="button"
    aria-disabled={disabled}
    onClick={!disabled ? onClick : undefined}
    {...rest}>
    {icon}
    {children}
  </div>
);

interface CheckboxProps extends DivProps {
  checked?: boolean;
  onChecked: (checked: boolean) => void;
}

export const CheckboxItem: React.FC<CheckboxProps>  = ({ className, checked, onChecked, onClick, children, ...rest }) => (
  <Item
    className={classNames('checkbox-item', className)}
    onClick={e => {
      // prevents checkbox input from firing second click event and closing the menu
      e.preventDefault();

      onChecked(!checked);
      onClick?.(e);
    }}
    {...rest}>
    <Checkbox checked={checked} onChecked={() => null} labelContent={children}/>
  </Item>
);

type Props = Partial<React.ComponentPropsWithoutRef<typeof PopUp>>;

export class ContextMenu extends Component<Props> {
  static Item = Item;
  static CheckboxItem = CheckboxItem;

  render(): React.ReactNode {
    const { className, trigger, title, popupClassName, ...rest } = this.props;
    return (
      <PopUp
        className={classNames('context-menu', className)}
        popupClassName={classNames('context-menu', popupClassName)}
        trigger={popUpState => (<>
          {!trigger && <Icons.KebabMenu
            appearsClickable
            activated={popUpState.isOpen}
            title={title ?? 'Open Menu'}
            className="context-menu-icon"/>}
          {trigger?.(popUpState)}
        </>)}
        {...rest}
      />
    );
  }
}
