Pop-up that blocks the rest of window content

 ```jsx
import { Button } from '../Button/Button';
import { Modal } from '../Modal/Modal';
import { Icons } from '../Icons/Icons';

const [isOpen, setIsOpen] = React.useState(false);

<div>
  <Button onClick={() => setIsOpen(true)}>Open</Button>
  <Modal headerIcon={<Icons.Connection/>} headerText="Connection Wizard" isOpen={isOpen} onRequestClose={() => setIsOpen(false)}>
    Click the "X" in the upper right hand corner to close this modal
  </Modal>
</div>
```

Variant with small size and a NoticeIcon in the body

 ```jsx
import { Button } from '../Button/Button';
import { ButtonRowConfirmCancel } from '../ButtonRow/ButtonRowConfirmCancel';
import { Modal } from '../Modal/Modal';
import { Icons } from '../Icons/Icons';

const [isOpen, setIsOpen] = React.useState(false);

<div>
  <Button onClick={() => setIsOpen(true)}>Open</Button>
  <Modal
    headerIcon={<Icons.Connection/>}
    headerText="Connection Wizard"
    buttons={<ButtonRowConfirmCancel
      onConfirm={() => setIsOpen(false)}
      onCancel={() => setIsOpen(false)}
    />}
    small
    bodyIcon="warning"
    isOpen={isOpen}
    onRequestClose={() => setIsOpen(false)}>
    Press "OK" to confirm
  </Modal>
</div>
```
