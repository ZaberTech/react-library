import React, { ReactElement, HTMLProps, useEffect, useRef } from 'react';
import classNames from 'classnames';

import { Icons } from '../Icons/Icons';
import { Text } from '../Text/Text';
import { SvgIconProps } from '../SvgIcon/SvgIcon';
import { ButtonRowProps } from '../ButtonRow/ButtonRow';
import { ButtonProps } from '../Button/Button';
import { ButtonRowConfirmCancelProps } from '../ButtonRow/ButtonRowConfirmCancel';
import { NoticeIcon } from '../Notice/NoticeIcon';
import { NoticeType } from '../Notice/Notice';
import { Loader } from '../Loader/Loader';

export type ModalButtons =
  ReactElement<ButtonRowConfirmCancelProps> |
  ReactElement<ButtonProps> |
  ReactElement<ButtonRowProps> |
  null |
  false;

export type ModalBodyIcon = NoticeType | 'loading';

interface ModalProps extends HTMLProps<HTMLDivElement> {
  headerIcon: ReactElement<SvgIconProps>;
  headerText: string;
  buttons?: ModalButtons;
  small?: boolean;
  bodyIcon?: ModalBodyIcon;
  isOpen?: boolean;
  onRequestClose?: () => void;
}

export const Modal: React.FC<ModalProps> = ({
  className,
  headerIcon,
  headerText,
  children,
  buttons,
  small,
  bodyIcon,
  isOpen = true,
  onRequestClose,
  ...rest
}) => <>
  {isOpen && <div className="modal-background-overlay"/>}
  <div className={classNames('modal', className, { small, hidden: !isOpen })} {...rest}>
    <div className="modal-header">
      {headerIcon}
      <Text t={Text.Type.H4}>{headerText}</Text>
      <Close onRequestClose={onRequestClose} isOpen={isOpen}/>
    </div>
    <div className="modal-body-wrapper">
      <div className="modal-body">
        {bodyIcon === 'loading' ?
          <Loader size="small" className="icon-body"/> :
          <NoticeIcon type={bodyIcon} className="icon-body"/>
        }
        <div className="modal-content">
          {children}
        </div>
      </div>
      {buttons &&
        <div className="buttons">
          {buttons}
        </div>
      }
    </div>
  </div>
</>;

type CloseProps = Pick<ModalProps, 'onRequestClose' | 'isOpen'>;

const Close: React.FC<CloseProps> = ({ onRequestClose, isOpen }) => {
  const requestClose = useRef(onRequestClose);
  requestClose.current = onRequestClose;

  useEffect(() => {
    if (!isOpen) { return }

    const onKeyDown = (event: KeyboardEvent) => {
      if (event.key === 'Escape') {
        event.preventDefault();
        requestClose.current?.();
      }
    };

    window.addEventListener('keydown', onKeyDown);
    return () => window.removeEventListener('keydown', onKeyDown);
  }, [isOpen]);

  if (!onRequestClose) { return null }

  return (
    <div className="modal-close">
      <Icons.Cross
        title="Close Dialog"
        interactionColor="white"
        onClick={() => onRequestClose()}/>
    </div>
  );
};
