import React from 'react';
import classNames from 'classnames';

type BaseButtonProps = React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>;
export type ButtonOnClick = BaseButtonProps['onClick'] | 'disabled';

export interface ButtonProps extends Omit<BaseButtonProps, 'onClick'> {
  /** Color of the body of the button */
  color?: 'red' | 'orange' | 'clear' | 'grey' | 'grey-red';
  /** Determines the size of the button */
  size?: 'large' | 'regular' | 'small';
  /** The action to handle clicks, or string literal `'disabled'` if the button should be disabled */
  onClick?: ButtonOnClick;
  /** Additional effects that apply to the button */
  effect?: 'pulse-shadow' | false;
}

/**
 * A generic styled button.
 */
export const Button: React.FC<ButtonProps> = ({
  color = 'red',
  className,
  size = 'regular',
  disabled: disabledPassed,
  onClick: onClickPassed,
  effect,
  ...buttonProps
}) => {
  const disabled = !!disabledPassed || onClickPassed === 'disabled';
  const onClick = onClickPassed === 'disabled' ? undefined : onClickPassed;
  return (
    <button
      {...buttonProps}
      className={classNames('button', { disabled }, className, color, size, effect)}
      disabled={disabled}
      onClick={onClick}
    />
  );
};
