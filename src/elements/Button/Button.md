Simple example:

```jsx
import { Button } from './Button';
<Button>Push Me</Button>
```

You can change the main color of the button:

```jsx padded
import { Button } from './Button';
<>
<Button color="red">I’m red!</Button>
<Button color="orange">I’m orange!</Button>
<Button color="grey">I’m grey!</Button>
<Button color="grey-red">I’m grey-red!</Button>
<Button color="clear">I’m clear!</Button>
</>
```

You can disable any color of button. The cursor will show as an "x" when hovering over,
and the colors will be grayed out.

```jsx padded
import { Button } from './Button';
<>
<Button color="red" disabled>I’m red!</Button>
<Button color="orange" disabled>I’m orange!</Button>
<Button color="grey" disabled>I’m grey!</Button>
<Button color="grey-red" disabled>I’m grey-red!</Button>
<Button color="clear" disabled>I’m clear!</Button>
</>
```

There are also different sizes:

```jsx padded
import { Button } from './Button';
<>
<Button size="large">I’m large!</Button>
<Button>I’m regular!</Button>
<Button size="small">I’m small!</Button>
</>
```

There is also pulsating effect to emphasize that something is happening and click may be needed:

```jsx padded
import { Button } from './Button';
<>
<Button effect="pulse-shadow" color="grey">I’m pulsating!</Button>
</>
```
