import React from 'react';
import classNames from 'classnames';

import { Icons } from '../Icons/Icons';

import { Button } from './Button';

type Props = React.ComponentProps<typeof Button>;

/**
 * An extention of Button class which displays an arrow to the left inside the content
 */
export const ButtonPrevious: React.FC<Props> = ({
  children,
  className,
  ...buttonProps
}) => (
  <Button className={classNames('button-arrow', className)} color="clear" {...buttonProps}>
    <Icons.ArrowLeft className="arrow left"/>
    {children}
  </Button>
);
