import React from 'react';
import classNames from 'classnames';

import { SvgIconProps } from '../SvgIcon/SvgIcon';
import { AnimationClasses } from '../../misc/animations/animations';
import { Icons } from '../Icons/Icons';

interface Props extends SvgIconProps {
  size?: 'large' | 'medium' | 'small';
}

export const Loader: React.FC<Props> = ({ className, size = 'medium', ...rest }) =>
  <Icons.Gear className={classNames('loader', size, className, AnimationClasses.Rotation)} {...rest}/>;
