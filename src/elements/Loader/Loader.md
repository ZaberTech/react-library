Component for entraining the user while waiting for a result of unnecesary long operation.

```jsx
<Loader/>
```

By default it comes in standard icon size. There is also a small version of 1.5rem and large version of 3rem.

```jsx
<Loader size="small"/>&emsp;
<Loader size="large"/>
```
