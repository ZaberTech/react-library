import classNames from 'classnames';
import React from 'react';
import _ from 'lodash';

type DivProps = React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLDivElement>, HTMLDivElement>;

const Dot: React.FC<{ selected: boolean }> = ({ selected }) => (
  <div className={classNames('dot', { selected })}/>
);

interface Props extends DivProps {
  count: number;
  current: number;
}

export const Steps: React.FC<Props> = ({ className, count, current, ...rest }) => (
  <div className={classNames('steps', className)} {...rest}>
    {_.range(count).map(i => <Dot key={i} selected={current === i}/>)}
  </div>
);
