import React, { Component } from 'react';
import classNames from 'classnames';

type SvgProps = React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLOrSVGElement>, HTMLOrSVGElement>;

export interface SvgIconProps extends Omit<SvgProps, 'onClick'> {
  /** Makes the icon look disabled and prevents onClick from being called. */
  disabled?: boolean;
  activated?: boolean;
  highlight?: 'on' | 'off';
  /** Only affects appearance (callbacks are still called). Defaults to true if onClick is provided. */
  appearsClickable?: boolean;
  interactionColor?: 'grey' | 'white';
  onClick?: SvgProps['onClick'] | 'disabled';
}

export function iconFromSvg(SvgComponent: React.ComponentType<SvgProps>): React.ComponentType<SvgIconProps> {
  class SvgIcon extends Component<SvgIconProps> {
    render(): React.ReactNode {
      const {
        appearsClickable: clickablePassed, onClick: onClickPassed,
        disabled: disabledPassed, activated, highlight, className, interactionColor = 'grey', ...rest
      } = this.props;
      const clickable = clickablePassed ?? (onClickPassed != null);
      const disabled = !!disabledPassed || (onClickPassed === 'disabled');
      const onClick = typeof onClickPassed === 'function' && !disabled ? onClickPassed : undefined;
      return <SvgComponent
        className={classNames('svg-icon', {
          clickable,
          disabled,
          activated,
          'highlight': highlight === 'on',
          'highlight-border': highlight != null,
        }, `interaction-${interactionColor}`, className)}
        role={clickable ? 'button' : 'img'}
        aria-disabled={clickable ? disabled : undefined}
        onClick={onClick}
        {...rest}
      />;
    }
  }
  return SvgIcon;
}
