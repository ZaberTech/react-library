CopyToClipboard allows you to make text copyable for the user. Children are rendered, and text from the `copyText` property is copied to the clipboard.

```jsx
import { CopyToClipboard } from '../CopyToClipboard/CopyToClipboard';
import { Text } from '../Text/Text';

<div>
  <CopyToClipboard copyText="Something funny here">
    <Text>Nothing funny here</Text>
  </CopyToClipboard>
  <br/>
  <br/>
  <input placeholder="Paste Here"/>
</div>
```

There is also a variant where the text is clickable for rare occasions when the icon is undesirable.

```jsx
import { CopyToClipboard } from '../CopyToClipboard/CopyToClipboard';
import { Text } from '../Text/Text';

<div>
  Click on the following text:
  <CopyToClipboard type="text"copyText="Something funny here">
    <Text>Copy Me</Text>
  </CopyToClipboard>
  <br/>
</div>
```
