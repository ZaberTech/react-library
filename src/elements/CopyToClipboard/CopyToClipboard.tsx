/* eslint-disable @typescript-eslint/no-misused-promises */
import classNames from 'classnames';
import React, { useEffect, useRef, useState, DetailedHTMLProps } from 'react';

import { Icons } from '../Icons/Icons';

const CLIPBOARD_MESSAGE_DURATION = 2500;

type ClipboardState =  'ready' | 'copied' | 'error';

interface CopyToClipboardProps extends DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  copyText: string;
  type?: 'icon' | 'text';
}

export const CopyToClipboard: React.FC<CopyToClipboardProps> = ({ copyText, type = 'icon', children, className, ...rest }) => {
  const [state, setState] = useState<ClipboardState>('ready');
  const timeoutRef = useRef<ReturnType<typeof setTimeout>>();

  const onCopy = async () => {
    try {
      await navigator.clipboard.writeText(copyText);
      setState('copied');
    } catch {
      setState('error');
    }

    timeoutRef.current = setTimeout(() => {
      timeoutRef.current = undefined;
      setState('ready');
    }, CLIPBOARD_MESSAGE_DURATION);
  };

  useEffect(() => () => {
    if (timeoutRef.current != null) {
      clearTimeout(timeoutRef.current);
    }
  }, []);

  return (
    <span {...rest} className={classNames('copy-to-clipboard', className, `state-${state}`, `type-${type}`)}>
      <span className="ready" onClick={type === 'text' && state === 'ready' ? onCopy : undefined}>
        {children}
      </span>
      <span className="copied">Copied to Clipboard</span>
      <span className="error">Clipboard Error</span>
      <Icons.Copy disabled={state !== 'ready'} className="copy-icon" onClick={onCopy}/>
    </span>
  );
};
