import React, { HTMLProps, useMemo } from 'react';
import classNames from 'classnames';
import { omit } from '@zaber/toolbox';
import _ from 'lodash';

import RadioBtnOn from '../../../assets/img/radio_bttn_on.svg';
import RadioBtnOff from '../../../assets/img/radio_bttn_off.svg';
import { iconFromSvg } from '../SvgIcon/SvgIcon';

const RadioOn = iconFromSvg(RadioBtnOn);
const RadioOff = iconFromSvg(RadioBtnOff);

interface Props extends HTMLProps<HTMLInputElement> {
  onValueChange?: (value: string) => void;
}

export const RadioButton: React.FC<Props> = props => {
  const { id, className, checked, children, disabled, ...rest } = props;
  const useId = useMemo(() => id ?? _.uniqueId('zaber-lib-radio'), [id]);

  const onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { onChange, onValueChange } = props;
    if (onChange) {
      onChange(e);
    }
    if (onValueChange) {
      onValueChange(e.target.value);
    }
  };

  return (
    <div className={classNames('radio-button', { disabled }, className)}>
      <input type="radio" id={useId} onChange={onChange} checked={checked}
        disabled={disabled} {...omit(rest, 'onChange', 'onValueChange')}/>
      <label htmlFor={useId}>
        {checked && <RadioOn className={classNames('radio-svg', 'checked')}/>}
        {!checked && <RadioOff className={classNames('radio-svg', 'unchecked')}/>}
        {children}
      </label>
    </div>
  );
};
