```jsx
const [value, setValue] = React.useState('');
<>
  <RadioButton
    name="connection-type"
    value="A"
    onValueChange={setValue}
    checked={value === 'A'}>Serial Port</RadioButton>
  <br/>
  <RadioButton
    name="connection-type"
    value="B"
    onValueChange={setValue}
    checked={value === 'B'}>Direct TCP/IP</RadioButton>
  <br/>
  <RadioButton
    disabled
    name="connection-type"
    value="C"
    onValueChange={setValue}
    checked={value === 'C'}>Disabled</RadioButton>
</>
```
