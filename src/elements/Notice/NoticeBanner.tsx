import React from 'react';
import classnames from 'classnames';

import { noticeColor } from './Notice';
import { NoticeMessage } from './NoticeMessage';

type Props = React.ComponentProps<typeof NoticeMessage>;

export const NoticeBanner: React.FC<Props> = ({ type = 'error', children, headline, collapsible, closer, className, ...rest }) => (
  <div className={classnames('notice-banner', className)} {...rest}>
    <div className="accent" style={{ backgroundColor: noticeColor[type] }}/>
    <NoticeMessage type={type} headline={headline} collapsible={collapsible} closer={closer}>{children}</NoticeMessage>
  </div>
);
