import classnames from 'classnames';
import React, { useEffect, useMemo, DetailedHTMLProps } from 'react';
import { createPortal } from 'react-dom';

import { NoticeType } from './Notice';
import { CloserProps, NoticeMessage } from './NoticeMessage';

interface Props extends DetailedHTMLProps<React.BaseHTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  type?: NoticeType;
  headline?: string;
  shown?: boolean;
  /** How long the toast should remain visible in ms. By default the toast remains until the user clears it. */
  duration?: number;
  /** By default, the toast is cleared with an "X" icon. You can also make this a button with props, or give it a title. */
  closer?: Omit<CloserProps, 'action'>;
  /** An action fired when either `duration` has passed, or the `closer` was clicked */
  onClose: () => void;
}

/** remove toaster from the DOM when the last toast is about to be cleared. */
function cleanToaster(toaster: Element | null) {
  if (toaster && toaster.children.length <= 1) {
    toaster.remove();
  }
}

export const Toast: React.FC<Props> = props => {
  const { shown = true, closer, duration, onClose, type, headline, children, className, ...rest } = props;
  const toaster = useMemo(() => {
    const toaster = document.querySelector('.zaber-toast-launcher');
    if (shown) {
      if (toaster != null) {
        return toaster;
      } else {
        const newToaster = document.createElement('div');
        newToaster.classList.add('zaber-toast-launcher');
        document.body.appendChild(newToaster);
        return newToaster;
      }
    } else {
      cleanToaster(toaster);
      return null;
    }
  }, [shown]);

  useEffect(() => (() => cleanToaster(toaster)), []);

  useEffect(() => {
    if (shown && duration != null) {
      const timeout = setTimeout(onClose, duration);
      return () => clearTimeout(timeout);
    }
    return () => undefined;
  }, [shown, duration]);

  const closerObj = { ...closer, action: onClose };

  if (!toaster || !shown) { return null }
  return createPortal(<div className={classnames('notice-toast', className)} {...rest}>
    <NoticeMessage type={type} headline={headline} closer={closerObj}>{children}</NoticeMessage>
  </div>, toaster);
};
