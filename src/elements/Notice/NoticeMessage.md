The `NoticeMessage` component is the base component the notice banner is built on. It can be used on its own when you want to
place it in something other than the normal banner

```jsx
import { NoticeBorder } from './NoticeBorder';

<NoticeBorder type="success">
  <div style={{ padding: '1rem 1.5rem' }}>
    <NoticeMessage type="error" headline="An Error Message with a Success Border">
      I have no idea why you'd want to do this, but you can!
    </NoticeMessage>
  </div>
</NoticeBorder>
```
