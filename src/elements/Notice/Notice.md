There are 4 Notice Types: `info`, `success`, `warning`, and `error`. These can be used to effect the color and icons
of the various notice components.
