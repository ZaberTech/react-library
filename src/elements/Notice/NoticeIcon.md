Use the `NoticeIcon` component to make an icon of the specified shape and color:

```jsx
  import { Flex } from '../../layout/Flex/Flex';

  <Flex.Row style={{ gap: '1rem' }}>
    <NoticeIcon type="info"/>
    <NoticeIcon type="warning"/>
    <NoticeIcon type="error"/>
    <NoticeIcon type="success"/>
  </Flex.Row>
```

You can pass the `colorless` prop if you don't want the icon to have it's usual color:

```jsx
  <NoticeIcon type="error" colorless/>
```
