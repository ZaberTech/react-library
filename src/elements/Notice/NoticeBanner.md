Use `NoticeBanner`s to tell users what's going on. By default, they show up as an `error`.
```jsx
  import { Flex } from '../../layout/Flex/Flex';

<Flex.Column style={{ alignItems: 'stretch', gap: '1rem' }}>
  <NoticeBanner>There has been a terrible failure. Lorem ipsum. Lorem ipsum. Lorem ipsum.</NoticeBanner>
  <NoticeBanner type="warning">There has been a some failure. Lorem ipsum. Lorem ipsum. Lorem ipsum.</NoticeBanner>
  <NoticeBanner type="info">Something has happened. Lorem ipsum. Lorem ipsum. Lorem ipsum.</NoticeBanner>
  <NoticeBanner type="success">
    Something good has happened. Lorem ipsum dolor sit amet, consectetur adipiscing elit,
    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
  </NoticeBanner>
</Flex.Column>
```

You can define an action to take to clear the notice:
```jsx
const style = { margin: '1rem' };

<div style={style}>
  <NoticeBanner closer={{ action: () => alert('Tried to clear the notice'), title: 'Coffee is for closers' }}>
    Click on the 'X' to see the callback prop in action
  </NoticeBanner>
</div>
```

This can be a button too, to make it clear exactly what will happen:
```jsx
const [foo, setFoo] = React.useState(true);

<div style={{ margin: '1rem' }}>
  {foo && (
  <NoticeBanner type="warning" closer={{ action: () => setFoo(false), button: 'Clear Foo' }}>
    Oh No, a Foo has occurred.
  </NoticeBanner>
  )}
  {!foo && (
  <NoticeBanner closer={{ action: () => setFoo(true), button: 'Clear BarBaz' }}>
    While trying to clear Foo, a BarBaz error was thrown!
  </NoticeBanner>
  )}
</div>
```

If you give the notice a `headline`, it will appear above the main message in the banner:
```jsx
<div style={{ margin: '1rem' }}>
  <NoticeBanner headline="Lorem ipsum" type="info" closer={() => {}}>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
      dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </NoticeBanner>
</div>
```

If you have a `headline` and the `collapsible` flag, the notice message is not visible until the user clicks "Show Details...":
```jsx
<div style={{ margin: '1rem' }}>
  <NoticeBanner headline="Lorem ipsum" type="info" collapsible>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
      dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </NoticeBanner>
</div>
```
