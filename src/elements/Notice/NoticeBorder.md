Lets you add a border around anything you want to apply notice to:

```jsx
  import { Select } from '../Input/Select';
  import { Icons } from '../Icons/Icons';
  import { noticeColor } from './Notice';
  import { NoticeIcon } from './NoticeIcon';

  const options = [
    ...['error', 'warning', 'info', 'success'].map(value => ({
      value,
      label: value.charAt(0).toUpperCase() + value.slice(1),
      icon: <NoticeIcon type={value} style={{ fontSize: '1.25rem' }}/>
      })),
    { value: null, label: 'None', icon: <Icons.Cloud/> },
  ];

  const [notice, setNotice] = React.useState(null);

  <NoticeBorder type={notice} style={{ width: 'fit-content' }}>
    <div style={{ width: '25rem', border: '1px solid grey', padding: '1rem 2rem' }}>
      <Select
        labelContent="Pick Yer Poison"
        value={notice}
        options={options}
        onChange={notice => setNotice(notice.value)}
      />
    </div>
  </NoticeBorder>
```

The border is placed over the top of the child content, rather than actually being a border for it.
This means that the `NoticeBorder` will never break the layout, but also that the child component
should ensure it has some extra padding, especially on the left, to compensate for the border
