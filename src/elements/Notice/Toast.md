Show a message at the bottom of the page that the user can clear at any time. It can be added at anywhere in a react component and it will
render itself at the bottom of the page:

```jsx
  import { Button } from '../Button/Button';

  const [shown, setShown] = React.useState(false);

  <div>
    <Button onClick={() => setShown(true)} disabled={shown}>Show Toast</Button>
    {shown && <Toast type="info" onClose={() => setShown(false)}>Hello, I am a toast</Toast>}
  </div>
```



You can also have toasts clear themselves after a period of time. If shown set to `false` the toast will be hidden:

```jsx
  import { Button } from '../Button/Button';
  import { Flex } from '../../layout/Flex/Flex';

  const [shown1, setShown1] = React.useState(false);
  const [shown2, setShown2] = React.useState(false);

  <Flex.Row style={{ gap: '1rem' }}>
    <Button onClick={() => setShown1(true)} disabled={shown1}>Show Toast 1</Button>
    <Toast type="info" shown={shown1} duration={4000} onClose={() => setShown1(false)}>
      This is a toast that will stick around 4 seconds (4000 ms).
    </Toast>

    <Button onClick={() => setShown2(true)} disabled={shown2}>Show Toast 2</Button>
    <Toast type="info" shown={shown2} duration={6000} onClose={() => setShown2(false)}>
      This toast will stick around for longer. 6 whole seconds!
    </Toast>
  </Flex.Row>
```
