import { Colors } from '../../misc/palette/Colors';

export type NoticeType = 'info' | 'success' | 'warning' | 'error';

export const noticeColor: Record<NoticeType, string> = {
  info: Colors.blue,
  success: Colors.green,
  warning: Colors.yellow,
  error: Colors.zaberRed,
};
