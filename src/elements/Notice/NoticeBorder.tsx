import React, { DetailedHTMLProps } from 'react';
import classNames from 'classnames';

import { noticeColor, NoticeType } from './Notice';

interface Props extends DetailedHTMLProps<React.BaseHTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  type?: NoticeType | null;
}

export const NoticeBorder: React.FC<Props> = ({ type, children, className, ...rest }) => (
  <div className={classNames('notice-border', className, type)} {...rest}>
    {type != null && <div className="notice-border-outline" style={{ borderColor: noticeColor[type] }}/>}
    {children}
  </div>
);
