/* eslint-disable react/display-name */
import React, { useState, DetailedHTMLProps, forwardRef } from 'react';
import classnames from 'classnames';
import { omit } from '@zaber/toolbox';

import { Text } from '../Text/Text';
import { Icons } from '../Icons/Icons';
import { Button } from '../Button/Button';
import { Flex } from '../../layout/Flex/Flex';

import { NoticeIcon } from './NoticeIcon';
import { NoticeType } from './Notice';

export type CloserProps = {
  action: () => void;
  /** The name of the button. If undefined, the closer will be rendered as a cross icon instead. */
  button?: string;
  title?: string;
};

const Closer: React.FC<CloserProps> = ({ action, button, title }) => {
  if (button == null) {
    return <Icons.Cross title={title} onClick={action}/>;
  } else {
    return <Button title={title} onClick={action} color="grey">{button}</Button>;
  }
};

interface Props extends DetailedHTMLProps<React.BaseHTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  type?: NoticeType;
  headline?: string;
  /** Whether this component's content can be hidden if there's a title available. By default `false`. */
  collapsible?: boolean;
  /** A function to call to dismiss this message or an object specifying the function as well as button text and title */
  closer?: CloserProps | (() => void);
}

export const NoticeMessage = forwardRef<HTMLDivElement, Props>(
  ({ type = 'error', headline, collapsible, closer, children, className, ...rest }, passedRef) => {
    const titleEmphasis = collapsible ? Text.Emphasis.Regular : Text.Emphasis.Bold;
    const [contentHidden, setContentHidden] = useState(!!headline && !!collapsible);
    const closerObject = typeof closer === 'function' ? { action: closer } : closer;

    return <div
      className={classnames('notice-message', className)}
      ref={passedRef}
      {...omit(rest, 'ref')}
    >
      <NoticeIcon type={type}/>
      {headline && <Flex.Row className="title">
        <Text e={titleEmphasis}>
          {headline}
        </Text>
        {collapsible && (
          <Text e={Text.Emphasis.Light} onClick={() => setContentHidden(!contentHidden)} className="toggle">
            {contentHidden ? 'Show' : 'Hide'} Details...
          </Text>
        )}
      </Flex.Row>}
      {!contentHidden &&
        <div className="content">
          {children}
        </div>
      }
      {closerObject != null && <div className="closer"><Closer {...closerObject}/></div>}
    </div>;
  });
