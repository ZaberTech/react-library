import classnames from 'classnames';
import React from 'react';

import { Icons } from '../Icons/Icons';
import { SvgIconProps } from '../SvgIcon/SvgIcon';

import { NoticeType } from './Notice';

interface Props extends Omit<SvgIconProps, 'type'> {
  type?: NoticeType | null;
  colorless?: boolean;
}

export const NoticeIcon: React.FC<Props> = ({ type, colorless, className, ...rest }) => {
  const classNames = classnames('notice-icon', type, { colorless }, className);

  return (
    type === 'error' ? <Icons.ErrorFault className={classNames} {...rest}/> :
    type === 'warning' ? <Icons.ErrorWarning className={classNames} {...rest}/> :
    type === 'info' ? <Icons.ErrorNote className={classNames} {...rest}/> :
    type === 'success' ? <Icons.Confirmation className={classNames} {...rest}/> :
    null
  );
};
