import Applications from '../../../assets/img/applications.svg';
import ArrowCollapsed from '../../../assets/img/chevron_right.svg';
import ArrowExpanded from '../../../assets/img/chevron_down.svg';
import ArrowLeft from '../../../assets/img/arrow_left.svg';
import ArrowRight from '../../../assets/img/arrow_right.svg';
import BasicMovement from '../../../assets/img/basic_controls.svg';
import Buffer from '../../../assets/img/buffer.svg';
import BulletPoint from '../../../assets/img/big-bullet.svg';
import BurgerMenu from '../../../assets/img/burger_menu.svg';
import Calendar from '../../../assets/img/calendar.svg';
import Chat from '../../../assets/img/contact_us.svg';
import ClearAll from '../../../assets/img/clear_all.svg';
import Cloud from '../../../assets/img/cloud.svg';
import Code from '../../../assets/img/code.svg';
import Computer from '../../../assets/img/computer.svg';
import Confirmation from '../../../assets/img/confirmation.svg';
import Connection from '../../../assets/img/share.svg';
import Copy from '../../../assets/img/copy.svg';
import Cross from '../../../assets/img/close.svg';
import CycleVelocity from '../../../assets/img/cycle_at_velocity.svg';
import Disable from '../../../assets/img/disable.svg';
import Disconnect from '../../../assets/img/disconnect.svg';
import Dot from '../../../assets/img/circle_big.svg';
import DotHollow from '../../../assets/img/dot_hollow.svg';
import DoubleGear from '../../../assets/img/device_settings.svg';
import Download from '../../../assets/img/download.svg';
import DropdownArrow from '../../../assets/img/nabla_down.svg';
import Edit from '../../../assets/img/edit.svg';
import Enable from '../../../assets/img/enable.svg';
import Enter from '../../../assets/img/send.svg';
import ErrorFault from '../../../assets/img/error_fault.svg';
import ErrorNote from '../../../assets/img/error_note.svg';
import ErrorWarning from '../../../assets/img/error_warning.svg';
import EthernetSocket from '../../../assets/img/tcp_ip.svg';
import Export from '../../../assets/img/export.svg';
import FactoryReset from '../../../assets/img/factory_reset.svg';
import Favourite from '../../../assets/img/favourite.svg';
import FavouriteFill from '../../../assets/img/favourite_fill.svg';
import Feedback from '../../../assets/img/feedback.svg';
import Filter from '../../../assets/img/filter.svg';
import Flash from '../../../assets/img/current_tuner.svg';
import Folder from '../../../assets/img/folder.svg';
import GCode from '../../../assets/img/gcode.svg';
import Gear from '../../../assets/img/loading.svg';
import HardwareModification from '../../../assets/img/hardware_modification.svg';
import Hide from '../../../assets/img/hide.svg';
import Home from '../../../assets/img/home.svg';
import Information from '../../../assets/img/information.svg';
import Joystick from '../../../assets/img/joystick.svg';
import KebabMenu from '../../../assets/img/kebab_menu.svg';
import Lc40 from '../../../assets/img/lc40.svg';
import Lc40WithNotification from '../../../assets/img/lc40_notification.svg';
import LeftEnd from '../../../assets/img/left-end.svg';
import LeftFast from '../../../assets/img/left-fast.svg';
import LeftNormal from '../../../assets/img/play_back.svg';
import Link from '../../../assets/img/link.svg';
import LoaderDots from '../../../assets/img/loader_dots.svg';
import Lock from '../../../assets/img/lock.svg';
import Lockstep from '../../../assets/img/lockstep.svg';
import Microscope from '../../../assets/img/microscope.svg';
import MoveToTopAndBottom from '../../../assets/img/top_bottom.svg';
import MoveUpAndDown from '../../../assets/img/up_down.svg';
import Mute from '../../../assets/img/no_sound.svg';
import Network from '../../../assets/img/network.svg';
import NewWindow from '../../../assets/img/new_window.svg';
import Notes from '../../../assets/img/notes.svg';
import Off from '../../../assets/img/off.svg';
import OpenFile from '../../../assets/img/open_file.svg';
import Oscilloscope from '../../../assets/img/oscilloscope.svg';
import Path from '../../../assets/img/path.svg';
import Pause from '../../../assets/img/pause.svg';
import Pin from '../../../assets/img/pin.svg';
import Pinned from '../../../assets/img/pinned.svg';
import Placeholder from '../../../assets/img/icon_placeholder.svg';
import PlayFromCursor from '../../../assets/img/play_from.svg';
import PlayOneLine from '../../../assets/img/play_one_line.svg';
import Plus from '../../../assets/img/plus.svg';
import Power from '../../../assets/img/power.svg';
import PVT from '../../../assets/img/pvt.svg';
import QuestionMark from '../../../assets/img/question.svg';
import Refresh from '../../../assets/img/refresh.svg';
import Restore from '../../../assets/img/restore.svg';
import RightEnd from '../../../assets/img/right-end.svg';
import RightFast from '../../../assets/img/right-fast.svg';
import RightNormal from '../../../assets/img/play.svg';
import Rotate from '../../../assets/img/rotate.svg';
import Save from '../../../assets/img/save.svg';
import SaveAs from '../../../assets/img/save_as.svg';
import SaveLoad from '../../../assets/img/save_load.svg';
import Scale from '../../../assets/img/scale.svg';
import Screwdriver from '../../../assets/img/screwdriver.svg';
import Search from '../../../assets/img/search.svg';
import ServoTuner from '../../../assets/img/servo_tuner.svg';
import Settings from '../../../assets/img/settings.svg';
import Shortcut from '../../../assets/img/shortcut.svg';
import Show from '../../../assets/img/show.svg';
import SignOut from '../../../assets/img/sign_out.svg';
import SoftwareUpdate from '../../../assets/img/software_update.svg';
import SoftwareUpdateWithNotification from '../../../assets/img/software_update_notification.svg';
import Sound from '../../../assets/img/sound.svg';
import Stop from '../../../assets/img/stop.svg';
import Support from '../../../assets/img/support.svg';
import Table from '../../../assets/img/table.svg';
import Terminal from '../../../assets/img/terminal.svg';
import Tick from '../../../assets/img/checkmark.svg';
import Trash from '../../../assets/img/trash.svg';
import Tutorials from '../../../assets/img/tutorials.svg';
import Unlock from '../../../assets/img/unlock.svg';
import Upgrade from '../../../assets/img/firmware_upgrader.svg';
import UpgradeWithNotification from '../../../assets/img/firmware_upgrader_notification.svg';
import Usb from '../../../assets/img/usb.svg';
import User from '../../../assets/img/user.svg';
import ValidateCode from '../../../assets/img/validate_code.svg';
import { iconFromSvg } from '../SvgIcon/SvgIcon';

export const Icons = {
  Applications: iconFromSvg(Applications),
  ArrowCollapsed: iconFromSvg(ArrowCollapsed),
  ArrowExpanded: iconFromSvg(ArrowExpanded),
  ArrowLeft: iconFromSvg(ArrowLeft),
  ArrowRight: iconFromSvg(ArrowRight),
  BasicMovement: iconFromSvg(BasicMovement),
  Buffer: iconFromSvg(Buffer),
  BulletPoint: iconFromSvg(BulletPoint),
  BurgerMenu: iconFromSvg(BurgerMenu),
  Calendar: iconFromSvg(Calendar),
  Chat: iconFromSvg(Chat),
  ClearAll: iconFromSvg(ClearAll),
  Cloud: iconFromSvg(Cloud),
  Code: iconFromSvg(Code),
  Computer: iconFromSvg(Computer),
  Confirmation: iconFromSvg(Confirmation),
  Connection: iconFromSvg(Connection),
  Copy: iconFromSvg(Copy),
  Cross: iconFromSvg(Cross),
  CycleVelocity: iconFromSvg(CycleVelocity),
  Disable: iconFromSvg(Disable),
  Disconnect: iconFromSvg(Disconnect),
  Dot: iconFromSvg(Dot),
  DotHollow: iconFromSvg(DotHollow),
  DoubleGear: iconFromSvg(DoubleGear),
  Download: iconFromSvg(Download),
  DropdownArrow: iconFromSvg(DropdownArrow),
  Edit: iconFromSvg(Edit),
  Enable: iconFromSvg(Enable),
  Enter: iconFromSvg(Enter),
  ErrorFault: iconFromSvg(ErrorFault),
  ErrorNote: iconFromSvg(ErrorNote),
  ErrorWarning: iconFromSvg(ErrorWarning),
  EthernetSocket: iconFromSvg(EthernetSocket),
  Export: iconFromSvg(Export),
  FactoryReset: iconFromSvg(FactoryReset),
  Favourite: iconFromSvg(Favourite),
  FavouriteFill: iconFromSvg(FavouriteFill),
  Feedback: iconFromSvg(Feedback),
  Filter: iconFromSvg(Filter),
  Flash: iconFromSvg(Flash),
  Folder: iconFromSvg(Folder),
  GCode: iconFromSvg(GCode),
  Gear: iconFromSvg(Gear),
  HardwareModification: iconFromSvg(HardwareModification),
  Hide: iconFromSvg(Hide),
  Home: iconFromSvg(Home),
  Information: iconFromSvg(Information),
  Joystick: iconFromSvg(Joystick),
  KebabMenu: iconFromSvg(KebabMenu),
  Lc40: iconFromSvg(Lc40),
  Lc40WithNotification: iconFromSvg(Lc40WithNotification),
  LeftEnd: iconFromSvg(LeftEnd),
  LeftFast: iconFromSvg(LeftFast),
  LeftNormal: iconFromSvg(LeftNormal),
  Link: iconFromSvg(Link),
  LoaderDots: iconFromSvg(LoaderDots),
  Lock: iconFromSvg(Lock),
  Lockstep: iconFromSvg(Lockstep),
  Microscope: iconFromSvg(Microscope),
  MoveToTopAndBottom: iconFromSvg(MoveToTopAndBottom),
  MoveUpAndDown: iconFromSvg(MoveUpAndDown),
  Mute: iconFromSvg(Mute),
  Network: iconFromSvg(Network),
  NewWindow: iconFromSvg(NewWindow),
  Notes: iconFromSvg(Notes),
  Off: iconFromSvg(Off),
  OpenFile: iconFromSvg(OpenFile),
  Oscilloscope: iconFromSvg(Oscilloscope),
  Path: iconFromSvg(Path),
  Pause: iconFromSvg(Pause),
  Pin: iconFromSvg(Pin),
  Pinned: iconFromSvg(Pinned),
  Placeholder: iconFromSvg(Placeholder),
  PlayFromCursor: iconFromSvg(PlayFromCursor),
  PlayOneLine: iconFromSvg(PlayOneLine),
  Plus: iconFromSvg(Plus),
  Power: iconFromSvg(Power),
  PVT: iconFromSvg(PVT),
  QuestionMark: iconFromSvg(QuestionMark),
  Refresh: iconFromSvg(Refresh),
  Restore: iconFromSvg(Restore),
  RightEnd: iconFromSvg(RightEnd),
  RightFast: iconFromSvg(RightFast),
  RightNormal: iconFromSvg(RightNormal),
  Rotate: iconFromSvg(Rotate),
  Save: iconFromSvg(Save),
  SaveAs: iconFromSvg(SaveAs),
  SaveLoad: iconFromSvg(SaveLoad),
  Scale: iconFromSvg(Scale),
  Screwdriver: iconFromSvg(Screwdriver),
  Search: iconFromSvg(Search),
  ServoTuner: iconFromSvg(ServoTuner),
  Settings: iconFromSvg(Settings),
  Shortcut: iconFromSvg(Shortcut),
  Show: iconFromSvg(Show),
  SignOut: iconFromSvg(SignOut),
  SoftwareUpdate: iconFromSvg(SoftwareUpdate),
  SoftwareUpdateWithNotification: iconFromSvg(SoftwareUpdateWithNotification),
  Sound: iconFromSvg(Sound),
  Stop: iconFromSvg(Stop),
  Support: iconFromSvg(Support),
  Table: iconFromSvg(Table),
  Terminal: iconFromSvg(Terminal),
  Tick: iconFromSvg(Tick),
  Trash: iconFromSvg(Trash),
  Tutorials: iconFromSvg(Tutorials),
  Unlock: iconFromSvg(Unlock),
  Upgrade: iconFromSvg(Upgrade),
  UpgradeWithNotification: iconFromSvg(UpgradeWithNotification),
  Usb: iconFromSvg(Usb),
  User: iconFromSvg(User),
  ValidateCode: iconFromSvg(ValidateCode),
};
