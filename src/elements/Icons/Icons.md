Point at the icon to learn its name.

``` jsx padded
import { Icons } from './Icons';

<React.Fragment>
  <div>Icons total: {Object.keys(Icons).length}</div>

  {Object.keys(Icons).map(name => {
    const Icon = Icons[name];
    return <span  key={name} title={name} style={{ fontSize: '2.25rem' }}><Icon /></span>;
  })}
</React.Fragment>
```


You can change the icon size using `font-size` property and color using `color` property.

``` jsx padded
import { Icons } from './Icons';

<React.Fragment>
  <Icons.Refresh style={{ fontSize: '8rem', color: 'magenta' }}/>
</React.Fragment>
```

Use clickable property to set appropriate cursor and make the icon pop on hover.
Most icons used as CTA will end-up with `font-size` set to `2.25rem`.
Additionally, you can provide `interactionColor` property to chose color of interaction.

``` jsx padded
import { Icons } from './Icons';

<React.Fragment>
  <Icons.Refresh style={{ fontSize: '2.25rem' }} clickable/>
  <div style={{ background: '#D32024', display: 'inline-block' }}>
    <Icons.Refresh style={{ fontSize: '2.25rem' }} clickable interactionColor="white"/>
  </div>
</React.Fragment>
```

If you are adding new icon, make sure that it has height proportional to the text.
This means that icon placed in text without specific font-size changes should look natural.
It will also ensure that all icons are of same size relative to each other.

The width of the icon can be arbitrary if needed, otherwise it should be same as height as most of the icons are square.
This will provide natural margin in the text and make the background circle look well if the icon is clickable.

Here are few examples:

``` jsx padded
import { Icons } from './Icons';

<React.Fragment>
  <p>Link with the following symbol <Icons.NewWindow style={{ margin: 0 }}/> opens new window.</p>
  <p>Icon <Icons.ErrorNote style={{ margin: 0, color: 'blue' }}/> indicates that there is more information available.</p>
  <p>Use <Icons.Placeholder style={{ margin: 0 }}/> if you don't have the right icon yet.</p>
</React.Fragment>
```

Icons can be highlighted to indicate that action is required.
However the halo is implemented using border and increases icon size by `4px`.
If you want the icon to maintain the size, have the border but not be highlighted, specify `off` (opposed to nullish value).

``` jsx padded
import { Icons } from './Icons';

<div style={{ border: '1px solid black' }}>
  <Icons.Refresh style={{ fontSize: '2.25rem' }} clickable highlight="on"/>
  <Icons.Refresh style={{ fontSize: '2.25rem' }} clickable highlight="off"/>
</div>
```
