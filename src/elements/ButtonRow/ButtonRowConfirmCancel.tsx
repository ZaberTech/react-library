import os from 'os';

import React from 'react';

import { ButtonOnClick, Button } from '../Button/Button';

import { ButtonRow, ButtonRowProps } from './ButtonRow';


export interface ButtonRowConfirmCancelProps extends ButtonRowProps {
    onConfirm: ButtonOnClick;
    onCancel: ButtonOnClick;
    confirmText?: string;
    cancelText?: string;
    order?: 'confirm-cancel' | 'cancel-confirm';
  }

export const ButtonRowConfirmCancel: React.FC<ButtonRowConfirmCancelProps> = ({
  justify = 'right',
  onConfirm,
  onCancel,
  confirmText = 'OK',
  cancelText = 'Cancel',
  order,
  children,
  ...rest
}) => {
  if (order == null) {
    if (['win32', 'browser'].includes(os.platform())) {
      order = 'confirm-cancel';
    } else {
      order = 'cancel-confirm';
    }
  }

  const confirmButton = <Button className="button confirm" title={confirmText} onClick={onConfirm}>{confirmText}</Button>;
  const cancelButton = <Button color="grey" className="button cancel" title={cancelText} onClick={onCancel}>{cancelText}</Button>;
  return <ButtonRow justify={justify} {...rest}>
    {['right', 'center'].includes(justify) && children}
    {order === 'confirm-cancel' ? confirmButton : cancelButton}
    {justify === 'spread' && children}
    {order === 'confirm-cancel' ? cancelButton : confirmButton}
    {justify === 'left' && children}
  </ButtonRow>;
};
