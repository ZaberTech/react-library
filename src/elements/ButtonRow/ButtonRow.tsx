import React, { HTMLAttributes } from 'react';
import classNames from 'classnames';


type Justify = 'left' | 'right' | 'center' | 'spread';
export interface ButtonRowProps extends HTMLAttributes<HTMLDivElement> {
  justify?: Justify;
}

export const ButtonRow: React.FC<ButtonRowProps> = ({ className, justify = 'right', children, ...rest }) =>
  <div className={classNames('button-row', className, `justify-${justify}`)} {...rest}>
    {children}
  </div>;
