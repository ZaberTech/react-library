A row of buttons with a Confirm button and a Cancel button.

Confirm and cancel buttons follow the OS default order in desktop applications. In a browser, the order is always `confirm` - `cancel`.

```jsx
<ButtonRowConfirmCancel onConfirm={() => alert('Confirmed!')} onCancel={() => alert('Cancelled!')}/>
```

Other buttons can optionally be passed as children, and will appear next to or between the Confirm and Cancel buttons (depending on justification).

```jsx
import { Button } from '../Button/Button';

<ButtonRowConfirmCancel justify="left" onConfirm={() => alert('Confirmed!')} onCancel={() => alert('Cancelled!')}>
  <Button>Button 1</Button>
</ButtonRowConfirmCancel>
```