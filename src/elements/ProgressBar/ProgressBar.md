Show a bar some percent completed:

```jsx
import { NumericInput } from '../Input/NumericInput';

const [prog, setProg] = React.useState(0.5);

<div style={{ width: '400px' }}>
  <NumericInput labelContent="Progress (0-1)" value={prog} onNumberChange={setProg}/>
  <ProgressBar progress={prog} style={{ width: '350px', margin: '2rem auto' }} />
</div>
```

Any value for `progress` greater than of equal to `1` results in a full bar:

```jsx
  <ProgressBar progress={42} style={{ width: '350px', margin: 'auto' }} />
```

Any value `0` or less results in an empty bar:

```jsx
  <ProgressBar progress={Number.NEGATIVE_INFINITY} style={{ width: '350px', margin: 'auto' }} />
```
