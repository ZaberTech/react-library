import classNames from 'classnames';
import React, { DetailedHTMLProps } from 'react';

import { Text } from '../Text/Text';

interface Props extends DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  /** The fraction completed. Must be between 0-1. */
  progress: number;
}

export const ProgressBar: React.FC<Props> = ({ progress, className, ...props }: Props) => {
  const progressNumber = Number.isFinite(progress) ? progress : 0;
  const boundedProgress = Math.max(Math.min(progressNumber, 1), 0);
  const percent = Math.floor(boundedProgress * 100);
  return (
    <div className={classNames('progress-bar', className)} {...props}>
      <span className="completed" style={{ width: `calc(1.5rem + ((100% - 1.5rem) * ${boundedProgress}))` }}>
        <Text t={Text.Type.Helper}>{`${percent}%`}</Text>
      </span>
    </div>
  );
};
