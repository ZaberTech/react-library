import _ from 'lodash';
import React, { useMemo, useState } from 'react';
import { Light as SyntaxHighlighter } from 'react-syntax-highlighter';
import defaultColorScheme from 'react-syntax-highlighter/dist/esm/styles/hljs/default-style';
import python from 'react-syntax-highlighter/dist/esm/languages/hljs/python';
import javascript from 'react-syntax-highlighter/dist/esm/languages/hljs/javascript';
import csharp from 'react-syntax-highlighter/dist/esm/languages/hljs/csharp';
import cpp from 'react-syntax-highlighter/dist/esm/languages/hljs/cpp';
import java from 'react-syntax-highlighter/dist/esm/languages/hljs/java';
import matlab from 'react-syntax-highlighter/dist/esm/languages/hljs/matlab';
import octave from 'react-syntax-highlighter/dist/esm/languages/hljs/c-like';
import classNames from 'classnames';
import { CopyToClipboard } from 'react-copy-to-clipboard';

import { Icons } from '../Icons/Icons';

const languages = { python, javascript, csharp, cpp, java, matlab, octave };

_.forEach(languages, (langFunc, lang) => {
  SyntaxHighlighter.registerLanguage(lang, langFunc);
});

export type Language = keyof typeof languages;
export type CodeSnippetTemplates = Partial<Record<Language, string>>;

export interface Props {
  templates: CodeSnippetTemplates;
  templateData?: Record<string, unknown>;
  colorScheme?: Record<string, React.CSSProperties>;
  initialLanguage?: Language;
  onLanguageSelected?: (language: Language) => void;
}


const languageTitles: Record<Language, string> = {
  python: 'Python',
  javascript: 'JavaScript',
  csharp: 'C#',
  cpp: 'C++',
  java: 'Java',
  matlab: 'MATLAB',
  octave: 'Octave',
};

const languageDisplayOrder: Language[] = ['python', 'csharp', 'javascript', 'cpp', 'java', 'matlab', 'octave'];

export const CodeSnippetGroup: React.FC<Props> =
  ({ templates, templateData, colorScheme, initialLanguage, onLanguageSelected }) => {
    const languageOrder = languageDisplayOrder.filter(lang => !!templates[lang]);
    const [currentLang, setCurrentLang] = useState(initialLanguage ?? languageOrder[0]);

    const compiledTemplates = useMemo(
      () => _.mapValues(templates, template => _.template(template, { interpolate: /<%=([\s\S]+?)%>/g })),
      [templates]
    );

    if (_.isEmpty(compiledTemplates)) {
      return null;
    }

    const currentLangText = compiledTemplates[currentLang]?.(templateData) ?? 'Invalid code template';

    return (<div className="code-snippet-group-container">
      <div className="title-container">
        {languageOrder.map(lang => <span
          key={lang}
          className={classNames('title', { active: currentLang === lang })}
          onClick={() => {
            setCurrentLang(lang);
            onLanguageSelected?.(lang);
          }}>
          {languageTitles[lang]}
        </span>)}
      </div>

      <div className="code-snippet">
        <CopyToClipboard text={currentLangText}>
          <Icons.Copy className="copy-icon" title="Copy to Clipboard"/>
        </CopyToClipboard>
        <SyntaxHighlighter language={currentLang} style={colorScheme ?? defaultColorScheme}>
          {currentLangText}
        </SyntaxHighlighter>
      </div>
    </div>);
  };
