Multi-language code snippet presenter. You must provide a dictionary of templates and a dictionary
of substitutions for rendering the template.

The props have an optional intitial language selection and language changed callback that can
be used to synchronize the language selection of multiple snippets.

```jsx
import examplePy from './example_snippets/example.py.template';
import exampleCs from './example_snippets/example.cs.template';
import exampleJs from './example_snippets/example.js.template';
import exampleCpp from './example_snippets/example.cpp.template';
import exampleJava from './example_snippets/example.java.template';
import exampleMatlab from './example_snippets/example.matlab.template';
import exampleOctave from './example_snippets/example.octave.template';

const templates = {
  python: examplePy,
  javascript: exampleJs,
  csharp: exampleCs,
  cpp: exampleCpp,
  java: exampleJava,
  matlab: exampleMatlab,
  octave: exampleOctave,
};

const templateData = {
  message: 'Hello, world!',
};

<CodeSnippetGroup templates={templates} templateData={templateData}/>
```

You can optionally override the default color scheme with an import from `react-syntax-highligher`:

```jsx
import dark from 'react-syntax-highlighter/dist/esm/styles/hljs/dark';

import examplePy from './example_snippets/example.py.template';
import exampleCs from './example_snippets/example.cs.template';
import exampleJs from './example_snippets/example.js.template';
import exampleCpp from './example_snippets/example.cpp.template';
import exampleJava from './example_snippets/example.java.template';
import exampleMatlab from './example_snippets/example.matlab.template';
import exampleOctave from './example_snippets/example.octave.template';

const templates = {
  python: examplePy,
  javascript: exampleJs,
  csharp: exampleCs,
  cpp: exampleCpp,
  java: exampleJava,
  matlab: exampleMatlab,
  octave: exampleOctave,
};

const templateData = {
  message: 'Hello, world!',
};

<CodeSnippetGroup templates={templates} templateData={templateData} colorScheme={dark}/>
```
